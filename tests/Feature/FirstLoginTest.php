<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use App\User;

class FirstLoginTest extends TestCase

{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCaminoUno()
    {        
        $user = User::create([
            'email' => 'emanuelmillan455@gmail.com',                                   
            'password' => Hash::make('020103yy'),
            'name' => 'Emanuel',
            'first_login' => false,
        ]);

        $this->post(route('login'), [
            'email' => $user->email,
            'password' => '020103yy'
        ]);    
            //dd($user->id);
        $response = $this->put('/user/3', [            
            'name'=>'nicolas',
            'id'=>'11928171167',
            'surnames'=>'lopez pineda',
            'id_type' =>'cedula de ciudadania',     
            'telephone'=>'3226174846',
            'sex'=>'masculino',
            'age'=>'19',
            'date_of_birth'=>'2000-07-26',
            'first_login'=>true
        ]);           
        $response->assertSessionHasNoErrors();
        $response->assertStatus(200);        
    }

    public function testCaminoDos()
    {
        $user = User::create([
            'email' => 'emanuelmillan455@gmail.com',                                   
            'password' => Hash::make('020103yy'),
            'name' => 'Emanuel',
            'first_login' => false,
        ]);

        $this->post(route('login'), [
            'email' => $user->email,
            'password' => '020103yy'
        ]);    
            //dd($user->id);
        $response = $this->put('/user/'.$user->id, [            
            'name'=>'',
            'id'=>'11928171167',
            'surnames'=>'lopez pineda',
            'id_type' =>'cedula de ciudadania',     
            'telephone'=>'3226174846',
            'sex'=>'masculino',
            'age'=>'19',
            'date_of_birth'=>'2000-07-26',
            'first_login'=>true
        ]);           


        $response->assertStatus(302);
        $response->assertSessionHasErrors('name');        
    }
}
