<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PlanCreationTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCaminoUno()
    {  
        $response = $this->post('/plans', [
            'name' => 'f',
            'price' => '60000',
            'wod' => true,
            'wod_online' => false
        ]);    

        $response->assertSessionHasNoErrors();
        $response->assertStatus(201);
    }

    public function testCaminoDos()
    {  
        $response = $this->post('/plans', [
            'name' => '',
            'price' => '60000',
            'wod' => true,
            'wod_online' => false
        ]);    

        $response->assertStatus(302);
    }
}
