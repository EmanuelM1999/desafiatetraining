<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserCreationTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCaminoUno()
    {
        $response = $this->post('/user', [
            'email' => 'nicolas@gmail.com',                                   
            'coach' => false,
            'administrador' => true,
            'atleta' => false,
            'plan_id' => null, 
            'first_login' => false,                        
        ]);                    

        $response->assertSessionHasNoErrors();
        $response->assertStatus(201);
    }

    public function testCaminoDos()
    {
        $response = $this->post('/user', [
            'email' => '',                                   
            'coach' => false,
            'administrador' => true,
            'atleta' => false,
            'plan_id' => null, 
            'first_login' => false,                        
        ]);                    

        $response->assertStatus(302);
    }
}
