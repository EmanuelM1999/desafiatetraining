<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use App\User;

class LoginTest extends TestCase
{    
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function testCaminoUno()
    {                
        for ($i=0; $i < 4 ; $i++) { 
            $this->post(route('login'), [
                'email' => 'pepito@gmail.com',
                'password' => '8765432mm'
            ]);
        }

        $response = $this->post(route('login'), [
            'email' => 'pepito@gmail.com',
            'password' => '8765432mm'
        ]);    
        $response->assertStatus(302);
        $response->assertSessionHasErrors('email');        
    }

    public function testCaminoDos()
    {
        $user = User::create([
            'email' => 'emanue1lmillan455@gmail.com',                                   
            'password' => Hash::make('020103yy'),
            'name' => 'Emanuel',
            'first_login' => true,
        ]);                

        $response = $this->post(route('login'), [
            'email' => $user->email,
            'password' => '020103yy'
        ]);                    

        $response->assertStatus(302);
        $response->assertRedirect('/home');
    }

    
    public function testCaminoTres()
    {
        $response = $this->post(route('login'), [
            'email' => 'pepito@gmail.com',
            'password' => '8765432mm'
        ]);    
        $response->assertStatus(302);
        $response->assertSessionHasErrors('email');        
    }

    public function testCaminoCuatro()
    {
        $response = $this->post(route('login'), [
            'email' => '',
            'password' => ''
        ]);    
        $response->assertStatus(302);
        $response->assertSessionHasErrors('email');        
    }
}
