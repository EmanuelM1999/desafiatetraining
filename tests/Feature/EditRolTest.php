<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use Illuminate\Support\Facades\Hash;

class EditRolTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCaminoUno()
    {
        
        $user = User::create([
            'email' => 'emanue1lmillan455@gmail.com',                                   
            'password' => Hash::make('020103yy'),
            'name' => 'Emanuel',
            'first_login' => true,
        ]);
        
        $response = $this->post('/update-rol', [
            'id' => $user->id,
            'administrador' => true,
            'coach' => false,
            'atleta' => false,                      
        ]);

        $response->assertSessionHasNoErrors();
        $response->assertStatus(200);
    }

    public function testCaminoDos()
    {              
        $user = User::create([
            'email' => 'emanue1lmillan455@gmail.com',                                   
            'password' => Hash::make('020103yy'),
            'name' => 'Emanuel',
            'first_login' => true,
        ]);

        $response = $this->post('/update-rol', [
            'id' => '3',
            'administrador' => true,
            'coach' => false,
            'atleta' => false,                      
        ]);        
        $response->assertStatus(404);
    }
}
