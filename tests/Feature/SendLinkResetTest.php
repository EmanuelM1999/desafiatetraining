<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use Illuminate\Support\Facades\Hash;

class SendLinkResetTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCaminoUno()
    {
        $user = User::create([
            'email' => 'emanuelmillan455@gmail.com',                                   
            'password' => Hash::make('020103yy'),
            'name' => 'Emanuel',
            'first_login' => true,
        ]);               
            
        $response = $this->post(route('password.email'),[
            'email' => $user->email,
        ]);        
        $response->assertSessionHasNoErrors();
        $response->assertStatus(302);                        
    }

    public function testCaminoDos()
    {
        $user = User::create([
            'email' => '',                                   
            'password' => Hash::make('020103yy'),
            'name' => 'Emanuel',
            'first_login' => true,
        ]);               
            
        $response = $this->post(route('password.email'),[
            'email' => $user->email,
        ]);

        $response->assertSessionHasErrors('email');        
        $response->assertStatus(302);                
    }
}
