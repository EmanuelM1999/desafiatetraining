<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SolicitudClaseTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCaminoUno()
    {
        $response = $this->post('/free-class-request', [
            'tipo' => 'Cedula de ciudadania',
            'id' => '1118311520',
            'nombre' => 'Gerson',
            'correo' => 'millan@prueba.com',
            'telefono' => '3206133896',
            'mensaje' => 'Espero respuesta'
        ]);           
        $response->assertSessionHasNoErrors();
        $response->assertStatus(200);
    }

    public function testCaminoDos()
    {
        $response = $this->post('/free-class-request', [
            'tipo' => 'Cedula de ciudadania',
            'id' => '',
            'nombre' => 'Gerson',
            'correo' => 'millan@prueba.com',
            'telefono' => '3206133896',
            'mensaje' => 'Espero respuesta'
        ]);           

        $response->assertStatus(302);
        $response->assertSessionHasErrors('id');        
    }
}
