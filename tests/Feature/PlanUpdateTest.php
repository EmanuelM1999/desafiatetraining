<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Plan;

class PlanUpdateTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCaminoUno()
    {
        $plan = Plan::Create( [
            'name' => 'plan',
            'price' => '60000',
            'wod' => true,
            'wod_online' => false
        ]);
            //dd($plan->id);
        $response = $this->put('/plans/'.$plan->id, [            
            'name' => 'plan2',
            'price' => '67000',
            'wod' => true,
            'wod_online' => false
        ]);
        $response->assertSessionHasNoErrors();
        $response->assertStatus(200);
    }

    public function testCaminoDos()
    {
        $plan = $this->post('/plans', [
            'name' => 'plan',
            'price' => '60000',
            'wod' => true,
            'wod_online' => false
        ]);

        $response = $this->put('/plans/2', [            
            'name' => '',
            'price' => '67000',
            'wod' => true,
            'wod_online' => false
        ]);
        
        $response->assertStatus(302);
    }
}
