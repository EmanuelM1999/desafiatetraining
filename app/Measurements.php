<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Measurements extends Model
{
    protected $fillable = ['date','weight', 'height', 'imc', 'fat', 'muscle', 'caloricExpenditure', 
    'metabolicAge', 'visceralFat', 'chest', 'highAbdomen', 'lowerAbdomen', 'waist', 'buttocks', 
    'rightLeg', 'leftLeg', 'rightCalf', 'leftCalf', 'relaxedRightArm', 'rightArmStressed', 
    'relaxedLeftArm', 'leftArmStressed', 'user_id'];
}
