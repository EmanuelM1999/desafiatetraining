<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','surnames','photo','id_type', 'id_document',
        'telephone','sex','age','date_of_birth',
        'visible_date_of_birth','home_address','city','departament',
        'country','weight','height','visible_weight',
        'visible_height','visible_benchmark_body','name_contact_emergency',
        'telephone_contact_emergency','email', 'administrador', 'coach', 
        'atleta', 'plan_id', 'payment_due_date', 'password', 'first_login', 
     ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
