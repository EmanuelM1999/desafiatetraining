<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassTemplate extends Model
{
    protected $fillable = ['name_template'];
}
