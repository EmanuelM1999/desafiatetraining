<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use App\Users;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Mail\Mailcreateuser;
use App\Mail\Mailfreeclass;
use Illuminate\Support\facades\Mail;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UpdateUserRequest;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * 
     * 
     */

    public function __construct()
    {        
    }


    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            return User::where('first_login', 1)->get();
        }else{
            return redirect('home');
        }
        
    }
    
    public function showInfoById(Request $request)
    {
        if ($request->wantsJson()) {
            return User::where('id', $request->id)->get();
        }else{
            return view('');
        }
        
    }
    
    public function getUserAuth(Request $request)
    {
        if ($request->wantsJson()) {
            return auth()->user();
        }else{
            return view('');
        }
        
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
 
     
        //Carácteres para la contraseña
         $str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
         $tpassword = '';
         //Reconstruimos la contraseña segun la longitud que se quiera
         for($i=0;$i<10;$i++) {
           //obtenemos un caracter aleatorio escogido de la cadena de caracteres
           $tpassword .= $str[rand(0,60)];
          }


        
        //Variable que guarda los datos que se enviaran en el correo
        $msg = [
                 'mail' => $request->email,
                 'password' =>$tpassword
               ];

                  Mail::to($request->email)->send(new Mailcreateuser($msg));

        
        return User::create([     
            'email' => $request->email,
            'administrador'=> $request->administrador,
            'coach'=> $request->coach,
            'atleta'=> $request->atleta,
            'plan_id'=> $request->plan_id,
            'password' =>Hash::make($tpassword),
            'first_login'=> $request->first_login                       
        ]);

       
        
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {    
        $user = auth()->user();   

        if($request->has('photo') && ($request->photo != null || $request->photo != "") && $request->photo!=$user->photo ){              

        $exploded = explode(',',$request->photo);

        $decoded = base64_decode($exploded[1]);

        if (str_contains($exploded[0],'jpeg')) {
            $extension ='jpg';
        }else{
            $extension ='png';
        }

        $fileName = str_random().'.'.$extension;

        $path = public_path().'/'.$fileName;

        file_put_contents($path,$decoded);

        if($user->photo != null){
            unlink(public_path($user->photo));
        }

        $user->photo = $fileName;        
        
    }    
        $user->name = $request->name;        
        $user->surnames = $request->surnames;              
        $user->telephone = $request->telephone;
        $user->sex = $request->sex;
        $user->age = $request->age;
        $user->date_of_birth = $request->date_of_birth;
        $user->first_login = $request->first_login;         
        $user->home_address = $request->home_address;
        $user->city = $request->city;
        $user->departament = $request->departament;
        $user->country = $request->country;
        $user->weight = $request->weight;
        $user->height = $request->height;
        $user->name_contact_emergency = $request->name_contact_emergency;
        $user->telephone_contact_emergency = $request->telephone_contact_emergency;    
        
        if ($request->password != null || $request->password != "" ) {
            $user->password = Hash::make($request->password);
        }

        $user->save();
    }
        
    public function updateRol(Request $request)
    {
        $user = User::findOrFail($request->id);
        $user->administrador = $request->administrador;
        $user->coach = $request->coach;
        $user->atleta = $request->atleta;
        $user->update();
        
        return $request;
    }

    public function searchAthletes(Request $request)
    {
        if ($request->wantsJson()) {
            return User::where('atleta', 1)->get();
        }else{
            return view('');
        }
    }
    
    public function ageUsers(Request $request)
    {

        if ($request->wantsJson()) {
            $ageusers = [];     
            $ageusers = \DB::table('users')->select('age')->get();
            return $ageusers;
        }else{
            return view('');
        }
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function queryCoaches()
    {
       $query = User::where('coach','1')->get();
        
       return $query;
    }
}
