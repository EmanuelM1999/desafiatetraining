<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use App\User;
use App\Http\Requests\PaymentStoreRequest;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            return Payment::where('user_id', auth()->id())->get();
        }else{
            return redirect('home');
        }
    }

     // method that lists the payments by user id

     public function listPayments(Request $request){
        if ($request->wantsJson()) {
            return Payment::where('user_id', $request->user_id)->get();
        }else{
            return view('');
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PaymentStoreRequest $request)
    {
        //update user's plan
        $user = User::find($request->user_id);
        $user->plan_id = $request->plan_id;
        $user->payment_due_date = $request->expirationDate;
        $user->update();
        //register the payment in the database
        return Payment::create([     
            'paymentDate' => $request->paymentDate,
            'expirationDate' => $request->expirationDate,
            'typePlan' => $request->typePlan,
            'planValue' => $request->planValue,
            'discountPayment' => $request->discountPayment,
            'total' => $request->total,
            'user_id' => $request->user_id
        ]);
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
