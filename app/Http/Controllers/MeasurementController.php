<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\MeasurementStoreRequest;
use App\Measurements;

class MeasurementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            return Measurements::where('user_id', auth()->id())->get();
        }else{
            return redirect('home');
        }
    }

        //method that lists measurements by user id

    public function listMeasurements(Request $request){
        if ($request->wantsJson()) {
            return Measurements::where('user_id', $request->user_id)->get();
        }else{
            return view('');
        }
        
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MeasurementStoreRequest $request)
    {
        return Measurements::create([     
            'date' => $request->date,
            'weight' => $request->weight,
            'height' => $request->height,
            'imc' => $request->imc,
            'fat' => $request->fat,
            'muscle' => $request->muscle,
            'caloricExpenditure' => $request->caloricExpenditure,
            'metabolicAge' => $request->metabolicAge,
            'visceralFat' => $request->visceralFat,
            'chest' => $request->chest,
            'highAbdomen' => $request->highAbdomen,
            'lowerAbdomen' => $request->lowerAbdomen,
            'waist' => $request->waist,
            'buttocks' => $request->buttocks,
            'rightLeg' => $request->rightLeg,
            'leftLeg' => $request->leftLeg,
            'rightCalf' => $request->rightCalf,
            'leftCalf' => $request->leftCalf,
            'relaxedRightArm' => $request->relaxedRightArm,
            'rightArmStressed' => $request->rightArmStressed,
            'relaxedLeftArm' => $request->relaxedLeftArm,
            'leftArmStressed' => $request->leftArmStressed,
            'user_id' => auth()->id()          
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
