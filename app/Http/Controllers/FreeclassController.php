<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\FreeclassRequest;
use Illuminate\Support\facades\Mail;
use App\Mail\Mailfreeclass;
use App\User;


class Freeclasscontroller extends Controller
{
    public function sendfreeclassmail(FreeclassRequest $request)
    {
        
      $msg = [
            'tipo' => $request->tipo,
            'id' => $request->id,
            'nombre' => $request->nombre,
            'correo' => $request->correo,
            'telefono' => $request->telefono,
            'mensaje' => $request->mensaje

          ];

       $correusu = [];     
       $correusu = \DB::table('users')->select('email')->where('coach', '=', 1)->get();
       
   
       foreach ($correusu as $email) {
           
           Mail::to($email->email)->send(new Mailfreeclass($msg));
           
    }
      
        return $request;
    }
}
