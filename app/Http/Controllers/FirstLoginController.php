<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\FirstLoginRequest;
use App\User;
use Illuminate\Support\Facades\Hash;

class FirstLoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->first_login == 0){
            return view('loginFirstTime');
        }else{
            return redirect('home');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {             
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FirstLoginRequest $request)
    {        
        $user = auth()->user();   

        if($request->has('photo')){              

        $exploded = explode(',',$request->photo);

        $decoded = base64_decode($exploded[1]);

        if (str_contains($exploded[0],'jpeg')) {
            $extension ='jpg';
        }else{
            $extension ='png';
        }

        $fileName = str_random().'.'.$extension;

        $path = public_path().'/'.$fileName;

        file_put_contents($path,$decoded);

        if($user->photo != null){
            unlink(public_path($user->photo));
        }

        $user->photo = $fileName;        
        
        }
    
        $user->name = $request->name;
        $user->id_document = $request->id;
        $user->surnames = $request->surnames;
        $user->id_type = $request->id_type;        
        $user->telephone = $request->telephone;
        $user->sex = $request->sex;
        $user->age = $request->age;
        $user->date_of_birth = $request->date_of_birth;
        $user->first_login = $request->first_login;         
        $user->home_address = $request->home_address;
        $user->city = $request->city;
        $user->departament = $request->departament;
        $user->country = $request->country;
        $user->weight = $request->weight;
        $user->height = $request->height;
        $user->name_contact_emergency = $request->name_contact_emergency;
        $user->telephone_contact_emergency = $request->telephone_contact_emergency;    
        $user->password = Hash::make($request->password);

        $user->save();
   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
