<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClassSchedule;
class ClassScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('home');   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ClassSchedule::create([
            'id_template' =>$request->id_template,
            'wod' =>$request->wod,
            'wod_online' =>$request->wod_online,
            'places' =>$request->places,
            'day' =>$request->day,
            'hour' =>$request->hour,
            'coach_id' =>$request->coach,

        ]);


        return $request;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $class = ClassSchedule::find($id);

        $class->wod = $request->wod;
        $class->wod_online = $request->wod_online;
        $class->places = $request->places;
        $class->day = $request->day;
        $class->hour = $request->hour;
        $class->coach_id = $request->coach;

        $class->save();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $class = ClassSchedule::find($id);
        $class->delete();
    }

    public function queryClasses(Request $request)
    {
        $classes = \DB::table('class_schedules')->select()->where('id_template', '=', $request->id)->orderBy('hour', 'asc')->get();

        return $classes;
        
    }
}
