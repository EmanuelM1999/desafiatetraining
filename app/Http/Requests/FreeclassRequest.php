<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FreeclassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'correo' => 'required|email|unique:users,email',
            'tipo' => 'required|string',
            'id' => 'required|integer|unique:users,id_document,'.NULL.',id,id_type,'.$this->tipo,
            'nombre' => 'required|string',
            'telefono' => 'required|string',
        ];
    }
}
