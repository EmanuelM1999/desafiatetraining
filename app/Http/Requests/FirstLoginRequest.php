<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FirstLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'photo' => 'required',
            'name' => 'required',
            'id' => 'required',
            'surnames' =>'required',
            'id_type' => 'required',        
            'telephone' =>'required' ,
            'sex' =>'required' ,
            'age' =>'required',
            'date_of_birth' =>'required',
            'first_login'=> 'required',
            'home_address'=>'required',
            'city'=>'required',
            'departament'=>'required',
            'country'=>'required',
            'weight'=>'required',
            'height'=>'required',
            'name_contact_emergency'=>'required',
            'telephone_contact_emergency'=>'required',
            'password'=>'required',
        ];
    }
}
