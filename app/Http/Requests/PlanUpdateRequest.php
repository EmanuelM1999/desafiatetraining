<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PlanUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        

        return [ 
            'name' => 'required|string|unique:plans,name,'.$this->id,
            'price' => 'required|integer|min:1',
            'wod' => 'required|boolean',
            'wod_online' => 'required|boolean',            
        ];
    }
    public function messages()
    {                
        return [
            'name.required' => 'El campo nombre es obligatorio.',
            'name.string' => 'El valor del campo nombre debe contener caracteres.',
            'name.unique' => 'El nombre de plan ingresado ya esta en uso.',
            'price.required' => 'El campo valor es obligatorio',
            'price.integer' => 'El campo valor debe de ser numerico',            
        ];
    }
}
