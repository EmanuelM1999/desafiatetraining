<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ObservationStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'class_date' => 'required|date|before:tomorrow',
            'description' => 'required|min:1|max:191|string'
        ];
    }

    public function messages()
    {
        return [
            'class_date.required' => 'El campo fecha es obligatorio',
            'class_date.before' => 'La fecha debe ser igual menor o igual a la actual',
            'description.require' => 'El campo descripcion es obligatorio',
            'description.min' => 'El campo descripcion debe contener minimo un caracter',
            'description.max' => 'El campo descripcion no puede contener mas de 191 caracteres',
            'description.string' => 'El campo descripcion debe ser de tipo texto',

        ];
    }
}
