<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\SingleMonthlyMeasure;

class MeasurementStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => ['required','date', new SingleMonthlyMeasure(),'before:tomorrow'],
            'weight' => 'required|min:1|numeric',
            'height' => 'required|min:1|numeric',
            'imc' => 'required|min:1|numeric',
            'fat' => 'required|min:1|numeric',
            'muscle' => 'required|min:1|numeric',
            'caloricExpenditure' => 'required|min:1|numeric',
            'metabolicAge' => 'required|min:1|numeric',
            'visceralFat' => 'required|min:1|numeric',
            'chest' => 'required|min:1|numeric',
            'highAbdomen' => 'required|min:1|numeric',
            'lowerAbdomen' => 'required|min:1|numeric',
            'waist' => 'required|min:1|numeric',
            'buttocks' => 'required|min:1|numeric',
            'rightLeg' => 'required|min:1|numeric',
            'leftLeg' => 'required|min:1|numeric',
            'rightCalf' => 'required|min:1|numeric',
            'leftCalf' => 'required|min:1|numeric',
            'relaxedRightArm' => 'required|min:1|numeric',
            'rightArmStressed' => 'required|min:1|numeric',
            'relaxedLeftArm' => 'required|min:1|numeric',
            'leftArmStressed' => 'required|min:1|numeric'
        ];
    }

    public function messages()
    {
        return [
            'date.require' => 'El campo fecha es obligatorio',
            'date.before' => 'La fecha debe ser igual menor o igual a la actual', 
            'weight.require' => 'El campo peso es obligatorio', 
            'weight.numeric' => 'El campo peso debe ser numerico',
            'height.require' => 'El campo estatura es obligatorio', 
            'height.numeric' => 'El campo estatura debe ser numerico',  
            'imc.require' => 'El campo imc es obligatorio', 
            'imc.numeric' => 'El campo imc debe ser numerico',  
            'fat.require' => 'El campo grasa es obligatorio', 
            'fat.numeric' => 'El campo grasa debe ser numerico',  
            'muscle.require' => 'El campo % de musculo es obligatorio', 
            'muscle.numeric' => 'El campo % de musculo debe ser numerico',  
            'caloricExpenditure.require' => 'El campo gasto calorico es obligatorio', 
            'caloricExpenditure.numeric' => 'El campo gasto calorico debe ser numerico',  
            'metabolicAget.require' => 'El campo edad metabolica es obligatorio', 
            'metabolicAge.numeric' => 'El campo edad metabolica debe ser numerico',
            'visceralFat.require' => 'El campo grasa visceral es obligatorio', 
            'visceralFat.numeric' => 'El campo grasa visceral debe ser numerico',  
            'chest.require' => 'El campo pecho es obligatorio', 
            'chest.numeric' => 'El campo pecho debe ser numerico',  
            'highAbdomen.require' => 'El campo abdomen alto es obligatorio', 
            'highAbdomen.numeric' => 'El campo abdomen alto debe ser numerico',  
            'lowerAbdomen.require' => 'El campo abdomen bajo es obligatorio', 
            'lowerAbdomen.numeric' => 'El campo abdoemn bajo debe ser numerico',  
            'waist.require' => 'El campo cintura es obligatorio', 
            'waist.numeric' => 'El campo cintura debe ser numerico',  
            'buttocks.require' => 'El campo glúteos es obligatorio', 
            'buttocks.numeric' => 'El campo glúteos debe ser numerico',  
            'rightLeg.require' => 'El campo pierna derecha es obligatorio', 
            'rightLeg.numeric' => 'El campo pierna derecha debe ser numerico',
            'leftLeg.require' => 'El campo pierna izquierda es obligatorio', 
            'leftLeg.numeric' => 'El campo pierna izquierda debe ser numerico',  
            'rightCalf.require' => 'El campo pantorrilla derecha es obligatorio', 
            'rightCalf.numeric' => 'El campo pantorrilla derecha debe ser numerico',  
            'leftCalf.require' => 'El campo pantorrilla izquierda es obligatorio', 
            'leftCalf.numeric' => 'El campo pantorrilla izquierda debe ser numerico',  
            'relaxedRightArm.require' => 'El campo brazo derecho relajado es obligatorio', 
            'relaxedRightArm.numeric' => 'El campo brazo derecho relajado debe ser numerico',  
            'rightArmStressed.require' => 'El campo brazo derecho tensionado es obligatorio', 
            'wrightArmStressed.numeric' => 'El campo brazo derecho tensionad debe ser numerico',  
            'relaxedLeftArm.require' => 'El campo brazo izquierdo relajado es obligatorio', 
            'relaxedLeftArm.numeric' => 'El campo brazo izquierdo relajado debe ser numerico',  
            'leftArmStressed.require' => 'El campo brazo izquierdo tensionado es obligatorio', 
            'leftArmStressed.numeric' => 'El campo brazo izquierdo tensionado debe ser numerico',  
                         
        ];
    }
  
}
