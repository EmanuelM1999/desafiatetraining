<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PlanStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

       
        return [ 
            'name' => 'required|unique:plans,name,'.$this->id,
            'price' => 'required',
            'wod' => 'required',
            'wod_online' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El campo nombre es obligatorio.',
            'name.string' => 'El valor del campo nombre debe contener caracteres.',
            'name.unique' => 'El nombre de plan ingresado ya esta en uso.',
            'price.required' => 'El campo valor es obligatorio',
            'price.integer' => 'El campo valor debe de ser numerico',
        ];
    }
}
