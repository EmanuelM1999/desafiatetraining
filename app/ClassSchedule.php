<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassSchedule extends Model
{
    protected $fillable = ['wod','wod_online','hour','day','id_template','coach_id','places'];
}
