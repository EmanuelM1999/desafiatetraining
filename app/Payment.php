<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['paymentDate', 'expirationDate', 'typePlan', 'planValue', 'discountPayment', 'total', 'user_id'];
}
