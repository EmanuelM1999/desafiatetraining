<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SingleMonthlyMeasure implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $date = new Carbon($value);

        $result = DB::select('SELECT * FROM measurements WHERE MONTH(date) = (:month) AND YEAR(date) = (:year) AND user_id = (:user_id)',
        ['month' =>$date->month ,
         'year' =>$date->year,
         'user_id' => auth()->id(),
        ]);

        //error_log($result);

        if(count($result) !=0){
            return false;
        }else{
            return true;
        }    
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Solo puede realizar un registro de medidas por mes.';
    }
}
