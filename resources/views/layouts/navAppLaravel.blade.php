<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Google Fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.18.0/css/mdb.min.css" rel="stylesheet">
    <style>
      #wrapper {
  overflow-x: hidden;
}

#sidebar-wrapper {
min-height: 100vh;
margin-left: -15rem;
-webkit-transition: margin .25s ease-out;
-moz-transition: margin .25s ease-out;
-o-transition: margin .25s ease-out;
transition: margin .25s ease-out;
}

#sidebar-wrapper .sidebar-heading {
padding: 0.875rem 1.25rem;
font-size: 1.2rem;
}

#sidebar-wrapper .list-group {
width: 15rem;
}

#page-content-wrapper {
min-width: 100vw;
}

#wrapper.toggled #sidebar-wrapper {
margin-left: 0;
}

@media (min-width: 768px) {
#sidebar-wrapper {
  margin-left: 0;
}

#page-content-wrapper {
  min-width: 0;
  width: 100%;
}

#wrapper.toggled #sidebar-wrapper {
  margin-left: -15rem;
}
}
#navbar-left {
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    font-weight: Bold;
    font-size: 20px;
    color: black;
    margin-left: auto;
    }
    .navbar-brand {
    font-family: Bangers; 
    font-weight: Bold; 
    font-size: 25px; 
    color: rgb(255, 0, 0)"
    }
    </style>
</head>
<body>
  <div id="app">

    <div class="d-flex" id="wrapper">

      <!-- Sidebar -->
      <div class="bg-light border-right" id="sidebar-wrapper">
          <div class="sidebar-heading">Menú Modulos</div>
        <div class="list-group list-group-flush" style="font-size: 16px;">
          <a id="sidebar-item" href="{{url('/dashboard')}}" class="list-group-item list-group-item-action bg-light">
          <i class="fas fa-user-circle fa-1x"></i>
          Perfil</a>
          <a id="sidebar-item" href="{{url('/userCreation')}}" class="list-group-item list-group-item-action bg-light">Crear Usuarios</a>
          <a id="sidebar-item" href="{{url('/planCreation')}}" class="list-group-item list-group-item-action bg-light">Planes</a>
          <a id="sidebar-item" href="{{url('/login')}}" class="list-group-item list-group-item-action bg-light">Cerrar sesión</a>
        </div>
      </div>
      <!-- /#sidebar-wrapper -->

      <!-- Page Content -->
      <div id="page-content-wrapper">

        <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
        <i class="fas fa-dumbbell fa-3x mx-3"></i>
          <a class="navbar-brand" href="#">Desafíate Training</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse container-fluid" id="navbarSupportedContent">
        
          </div>
        </nav>
      <main class="my-4">
          @yield('content')
      </main>
      </div>
      <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

  </div>
    <!-- JQuery -->
<!-- JQuery -->
<script type="text/javascript" src="js/jquery-3.4.1.min-js">
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.18.0/js/mdb.min.js"></script>

</body>
</html>

