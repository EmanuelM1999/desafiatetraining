
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Desafiate Training</title>
    <link rel="icon" href="imagesProject/logoDesafiateTraining.png" type="image/png">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles --> 
   
    <script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">

  <style>
    .color-nav{
        background: #005AA7;  /* fallback for old browsers */
        background: -webkit-linear-gradient(to right, #005AA7, #FFFDE4);  /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to right, #005AA7, #FFFDE4); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
      }
    .navbar-left {
      font-family: 'Open Sans', sans-serif;
      font-weight: Bold;
      font-size: 20px;
      color: red;
      margin-left: auto;
    }
    .navbar-brand {
      font-family: Bangers;  
      font-weight: Bold; 
      font-size: 25px; 
      color: rgb(255, 0, 0)"
    }
    .fondo{
      background-color: #63a4ff;
      background-image: linear-gradient(315deg, #63a4ff 0%, #83eaf1 74%);
    }
    .img-navAppPage{
            height: 45px;
            animation:none;
          }
  </style>

</head>
<body >
  
</style>
  <div id="app" class="">
    <nav class="navbar-expand-lg navbar color-nav"> 
    <a class="navbar-brand black-text" href="{{url('/')}}"><img class="img-navAppPage d-inline-block" src="imagesProject/logoDesafiateTraining.png" alt="">Desafíate Training</a>
      <button class="navbar-toggler btn-primary" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="mx-5 collapse navbar-collapse" id="navbarNav">
        
      </div>
    </nav>
    <main class="my-4">    
        @yield('content')
    </main>
  </div>
</body>
</html>
