
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Desafiate Training</title>
    <link rel="icon" href="imagesProject/logoDesafiateTraining.png" type="image/png">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles --> 
   
    <script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/StylesHomePage/navAppPage.css') }}" rel="stylesheet">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
</head>
<body >
  
</style>
  <div id="app" class="">
    <nav class="navbar-expand-lg navbar">
      <a class="mx-5 navbar-brand" href="{{url('/')}}"> 
     </a>
      <button class="navbar-toggler btn-primary" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="mx-5 collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav navbar-left" style="color:black">
        <li class="nav-item">
            <a class="nav-link" href="{{url('/')}}">Inicio</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('/home-plans')}}">Planes</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('/home-coach')}}">Coaches</a>
          </li>
        </ul>
        <form class="form-inline">
          <a class="btn btn-outline-primary px-2 py-2 " type="button" href="{{route('login')}}">Inicio de sesión</a>
        </form>
      </div>
    </nav>
    <main class="">        
        @yield('content')
    </main>
  </div>
  
</body>
</html>

