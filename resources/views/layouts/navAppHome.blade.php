<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Desafíate Training</title>
    <link rel="icon" href="imagesProject/logoDesafiateTraining.png" type="image/png">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles --> 
   
    <script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/navAppHome.css') }}" rel="stylesheet">
    <link href="{{ asset('css/UsersModule/UsersList.css') }}" rel="stylesheet">
    <link href="{{ asset('css/ClassesModule/reserveClass.css') }}" rel="stylesheet">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
</head>
<body class="" style="background:rgb(245, 245, 245);">

  <div id="app">

    <div class="d-flex" id="wrapper">

      <!-- Sidebar -->
      <div class="bg-white border-right fixed-left scrollbar-inner" id="sidebar-wrapper">
          <!-- <div class="sidebar-heading">Menú</div> -->
        <div class="list-group list-group-flush" style="font-size: 16px;">
          <!-- <a id="sidebar-item" href="{{url('/dashboard')}}" class="list-group-item list-group-item-action bg-light">
          <i class="fas fa-user-circle fa-1x"></i>
          Perfil</a>           -->
          <router-link id="sidebar-item" to="/my-profile" class="list-group-item list-group-item-action bg-white" tooltip="hola">
          <p class="font-weight-bold"><span class="text-primary">Bienvenido.</span><br> {{auth()->user()->name}} {{auth()->user()->surnames}}</p>
          <img src="{{auth()->user()->photo}}" alt="" class="img-profile mx-auto">
          </router-link>  
          @if(auth()->check() && (auth()->user()->administrador == 1 || auth()->user()->coach == 1))
          <router-link id="sidebar-item" to="/users-module" class="list-group-item list-group-item-action bg-white"><i class="fas fa-users mx-2"></i>Usuarios</router-link>  
          @endif
          @if(auth()->check() && auth()->user()->administrador == 1) 
          <router-link id="sidebar-item" to="/weekly-classes" class="list-group-item list-group-item-action bg-white" style="font-size: 14px"><i class="fas fa-calendar-alt mx-2"></i>Plantillas de clases</router-link>
          <router-link id="sidebar-item" to="/plan-module" class="list-group-item list-group-item-action bg-white"><i class="fas fa-tags mx-2"></i>Planes</router-link>
          <router-link id="sidebar-item" to="/reports-module" class="list-group-item list-group-item-action bg-white"><i class="fas fa-chart-pie mx-2"></i>Informes</router-link>
          @endif
          @if(auth()->check() && (auth()->user()->administrador == 1 || auth()->user()->coach == 1 || auth()->user()->atleta == 1))
          <router-link id="sidebar-item" to="/reserve-class" class="list-group-item list-group-item-action bg-white"><i class="fas fa-calendar-check mx-2"></i>Reservar clases</router-link>
          <router-link id="sidebar-item" to="/my-body" class="list-group-item list-group-item-action bg-white"><i class="fas fa-child mx-2"></i>Mi cuerpo</router-link>
          <router-link id="sidebar-item" to="/my-profile" class="list-group-item list-group-item-action bg-white"><i class="fas fa-user mx-2"></i>Perfil</router-link>          
          <router-link id="sidebar-item" to="/list-payments" class="list-group-item list-group-item-action bg-white"><i class="fas fa-dollar-sign mx-2"></i>Pagos</router-link>
          <router-link id="sidebar-item" to="/user-observation" class="list-group-item list-group-item-action bg-white"><i class="fas fa-eye mx-2"></i>Observaciones</router-link>
          @endif
          <div>
          <a id="sidebar-item" href="{{route('logout')}}"
                   onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();"
                          class="list-group-item list-group-item-action bg-white"><i class="fas fa-door-open mx-2"></i>Cerrar sesión</a>

         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>
           </div>
        </div>
      </div>
      <!-- /#sidebar-wrapper -->

      <!-- Page Content -->
      <div id="page-content-wrapper">

        <nav class="navbar navbar-expand-lg color-nav bg-light">
          <a class="navbar-brand black-text" href="#"><img class="img-navAppPage d-inline-block" src="imagesProject/logoDesafiateTraining.png" alt="">Desafíate Training</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse container-fluid" id="navbarSupportedContent">
        
          </div>
        </nav>
        
        <v-app>
          <v-main>
            <router-view></router-view>
          </v-main>
      </v-app>  
        
      </div>
      <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

  </div>
  
</body>
</html>

<style>

.img-profile{
  animation:none;
  width:150px;
  height:100px;
}

</style>