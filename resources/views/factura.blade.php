<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Recibo de pago</title>
    
<body>
    <div class="container my-5">
        <form class="list-group-item p-4"> 
        <div class="row">
        <label class="font-weight-bold cols-12 col-lg-12">Desafíate Training caf</label>
        <label class="col-md-2 font-weight-bold" style="position:fixed;left:950px;top:70px">RECIBO</label>
        <label class="font-weight-bold cols-12 col-lg-12">1118295663-8</label>
        <label class="col-md-2 font-weight-bold" style="position:fixed;left:800px;top:130px"><label class="col-sm-6 font-weight-bold p-1 text-center" style="background:#5198FF">Fecha</label></label>
        <label class="col-md-2 font-weight-bold" style="position:fixed;left:900px;top:130px"><label class="col-sm-6 bg-light font-weight-bold p-1 text-center">19/06/2020</label></label> 
        <label class="font-weight-bold cols-12 col-lg-12">Calle 8 # 10-28</label>
        <label class="font-weight-bold cols-12 col-lg-12">760501, Yumbo, Valle del Cauca, Colombia</label>
        </div>  

<table class="table my-3">
  <thead class="" style="background:#5198FF">
    <tr>
      <th scope="col" class="font-weight-bold">Fecha de pago</th>
      <th scope="col" class="font-weight-bold">Fecha de vencimiento</th>
      <th scope="col" class="font-weight-bold">Plan</th>
      <th scope="col" class="font-weight-bold">Valor del plan</th>
      <th scope="col" class="font-weight-bold">Descuento aplicado</th>
      <th scope="col" class="font-weight-bold">Total</th>
  
      
    </tr>
  </thead>
  <tbody>
     @foreach($payment as $pay)
    <tr>
    <td>{{ $pay->paymentDate }}</td>
    <td>{{ $pay->expirationDate }}</td>
    <td>{{ $pay->typePlan }}</td>
    <td>{{ $pay->planValue }}</td>
    <td>{{ $pay->discountPayment }}</td>
    <td>{{ $pay->total }}</td>
    </tr>
    @endforeach
  </tbody>
</table>
        </form>
        
    </div>
    
</body>
</html>
