@extends('layouts.navPrueba')

@section('content')
<style>
    .container-form{
        width: 500px;
        height: auto;
        background: white;;
        margin: auto;
        padding: 20px;
        box-shadow: -2px -2px 2px 1px rgba(0, 0, 0, 0.2), 2px 2px 2px 1px rgba(0, 0, 0, 0.2);
        font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    }
</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="cols-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">{{ __('Restablecer contraseña') }}</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-12 col-lg-12">{{ __('Direccion de correo electronico') }}</label>

                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group" style="margin:auto;">
                                <button type="submit" class="btn blue-gradient">
                                    {{ __('Restablecer contraseña') }}
                                </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
