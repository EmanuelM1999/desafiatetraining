@extends('layouts.navPrueba')

@section('content')

<div class="container-form">
    <div class="row justify-content-center">
        <div class="cols-12 col-md-12 col-lg-12">
            <form class="text-center border border-light p-5 cols-12 col-lg-12" method="POST" action="{{ route('login') }}">
            @csrf
                <p class="h4 mb-4">Inicio de sesión</p>

                <!-- Email -->
                <div class="form-group">
                    <input id="email" placeholder="E-mail" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                    <label for="email"></label>    
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>
                
                <!-- Password -->
                <div class="form-group">
                    <input id="password" placeholder="Contraseña" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                    <label for="password"></label>
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                

                <div class="d-flex justify-content-around">
                    <div>
                        <!-- Remember me -->
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="custom-control-label" for="remember">Recuerdame</label>
                        </div>
                    </div>
                    <div>
                        <!-- Forgot password -->
                        @if (Route::has('password.request'))
                            <a href="{{ route('password.request') }}">Olvidó contraseña?</a>
                        @endif
                    </div>
                </div>

                <!-- Sign in button -->
                <button class="btn btn-info btn-block my-4" type="submit">Iniciar Sesión</button>

                <!-- Register -->
                <p>No eres un miembro?
                    <a href="">Apúntate ahora</a>
                </p>
                <a href="https://www.facebook.com/desafiatetraining/" target="_blank" class="mx-2" role="button"><i class="fab fa-facebook-f light-blue-text"></i></a>
                <a href="https://www.instagram.com/desafiatetraining/" target="_blank" class="mx-2" role="button"><i class="fab fa-instagram light-blue-text"></i></a>

            </form>
        <!-- Default form login -->
        </div>
        <!-- Default form login -->
    </div>
</div>
@endsection
<style>
    .container-form{
        width: 500px;
        height: auto;
        background: white;;
        margin: auto;
        padding: 20px;
        box-shadow: -2px -2px 2px 1px rgba(0, 0, 0, 0.2), 2px 2px 2px 1px rgba(0, 0, 0, 0.2);
        font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    }
</style>