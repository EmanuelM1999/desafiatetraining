
  //state
  const state = () => ({
      errors:[],
    snackbarC:{
        visible: false,
        message:'',
        color:'',
        icon:''
    },
  })
  
  // getters
  const getters = {
    getSnackbarC(state){
        return state.snackbarC
    }
  }
  
  // actions
  const actions = {
    createRequest({commit,state},data){
        axios.post('/free-class-request', {
            tipo: data.typeId,
            id: data.id,
            nombre: data.name,
            correo: data.email,
            telefono: data.telephone,
            mensaje: data.message
            
        })
        .then(response => {                                
            console.log(response.status);   
                if(response.status == 200){
                    let successMsg = {
                        visible : true,
                        message : 'Solicitud enviada con éxito',
                        color : 'success',
                        icon : 'mdi-checkbox-marked-circle'
                      }
                        
                        commit('setSnackbarC',successMsg);
                } 
        })
        .catch(error => {
            state.errors = error.response.data.errors
                if(error.response.status == 422){
                    let errorMsg = {
                        visible : true,
                        message : state.errors.correo[0],
                        color : 'error',
                        icon : 'mdi-close-octagon'
                      }
                        
                        commit('setSnackbarC',errorMsg);
                        
                }
        });
    }
  }
   
  // mutations
  const mutations = {
    setSnackbarC(state, msg){
        state.snackbarC.visible = false
        state.snackbarC.visible = msg.visible
        state.snackbarC.message = msg.message
        state.snackbarC.color = msg.color
        state.snackbarC.icon = msg.icon
    },
  }
  
  export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
  }