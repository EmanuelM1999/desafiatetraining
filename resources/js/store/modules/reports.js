const state = () => ({
    usersAthletes:[],
    ages:[],
    plans:[],
    plansName:[],
    usersbyage:[],
    agerange:['12-17', '18-24', '25-34', '35-44', '45+'],
    usersbyplan:[]
})

const getters = {
    getAthletes(state){
        return state.usersAthletes
    },
    getPlans(state){
        return state.usersbyplan
    },
    getPlansName(state){
        return state.plansName
    },
    getAges(state){
        return state.usersbyage
    }
}

const actions = {
    getUsersAthletes({commit}){
        axios.get('/list-athletes')
            .then(response => {
                commit('setUsersAthletes',response.data)
            })
            .catch(function (error) {
                 console.log(error);
            });

    },
    getAllPlans({commit}){
             axios.get('/plans')
            .then(response => {
                commit('setPlans',response.data)
            })
            .catch(function (error) {
                 console.log(error);
            });

    },
    getUsersAges({commit}){
             axios.get('/age-users')
            .then(response => {
                commit('setAges',response.data)
            })
            .catch(function (error) {
                 console.log(error);
            });

    },
}

const mutations = {
    setUsersAthletes(state,users){
        state.usersAthletes = users
    },
    setPlans(state,plans){
        state.plans = plans
        state.plansName = []

        for (let i = 0; i < state.plans.length; i++) {
            state.plansName.push(state.plans[i].name)
        }
        
        for (let i = 0; i < state.plans.length; i++) {
            state.usersbyplan[i] = 0
        }

        for(let i=0 ; i < state.plans.length ; i++){
              
              for (let j = 0; j < state.usersAthletes.length; j++) {    
                    if (state.usersAthletes[j].plan_id == state.plans[i].id) { 
                        state.usersbyplan[i] += 1
                    }
              }
              
         }
    },
    setAges(state,ages){
        state.usersbyage = []
        state.ages = ages
        for (let index = 0; index < state.agerange.length; index++) {
            state.usersbyage[index] = 0
        } 
               
        for (let i = 0; i < state.ages.length; i++) {
             if (state.ages[i].age >= 12 && state.ages[i].age <= 17) {
                state.usersbyage[0] += 1
             }else if(state.ages[i].age >= 18 && state.ages[i].age <= 24){
                state.usersbyage[1] += 1
             }else if(state.ages[i].age >= 25 && state.ages[i].age <= 34){
                state.usersbyage[2] += 1
             }else if(state.ages[i].age >= 35 && state.ages[i].age <= 44){
                state.usersbyage[3] += 1
             }else if(state.ages[i].age >= 45){
                state.usersbyage[4] += 1
             }
        }
        
    }
    
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}