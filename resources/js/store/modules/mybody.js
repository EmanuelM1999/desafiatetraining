import axios from 'axios';

  //state
  const state = () => ({
    measurements:[],
    measurementInfo:{dateInfo:'',weightInfo:'', heightInfo:'', imcInfo:'', fatInfo:'', muscleInfo:'', caloricExpenditureInfo:'', 
            metabolicAgeInfo:'', visceralFatInfo:'', chestInfo:'', highAbdomenInfo:'', lowerAbdomenInfo:'', waistInfo:'', buttocksInfo:'', 
            rightLegInfo:'', leftLegInfo:'', rightCalfInfo:'', leftCalfInfo:'', relaxedRightArmInfo:'', rightArmStressedInfo:'', 
            relaxedLeftArmInfo:'', leftArmStressedInfo:''},
    snackbar:{
      visible: false,
      message:'',
      color:'',
      icon:''
    },
    dialog:false,
    errors:[]
  })
  
  // getters
  const getters = {
    getmyMeasurements(state){
      return state.measurements
    },
    getSnackbar(state){
      return state.snackbar
    },
    getD(state){
      return state.dialog
    },
    getMeasurementInfo(state){
      return state.measurementInfo
    }
  }
  
  // actions
  const actions = {
    getMyMeasurements ({ commit }) {
        axios.get('/measurements')
        .then(response => {
             commit('setMyMeasurements',response.data)    
        })
    },//FIN
    addMeasurementRecord ({commit,dispatch,state},data){
            axios.post('/measurements', {
            date: data.date,
            weight: data.weight,
            height: data.height,
            imc:  data.imc,
            fat: data.fat,
            muscle: data.muscle,
            caloricExpenditure: data.caloricExpenditure,
            metabolicAge: data.metabolicAge,
            visceralFat: data.visceralFat,
            chest: data.chest,
            highAbdomen: data.highAbdomen,
            lowerAbdomen: data.lowerAbdomen,
            waist: data.waist,
            buttocks: data.buttocks,
            rightLeg: data.rightLeg,
            leftLeg: data.leftLeg,
            rightCalf: data.rightCalf,
            leftCalf: data.leftCalf ,
            relaxedRightArm: data.relaxedRightArm,
            rightArmStressed: data.rightArmStressed,
            relaxedLeftArm: data.relaxedLeftArm,
            leftArmStressed: data.leftArmStressed
                                    
                })
                .then(response => {
                    if(response.status == 201){
                      let successMsg = {
                        visible : true,
                        message : 'Registro exitoso',
                        color : 'success',
                        icon : 'mdi-checkbox-marked-circle'
                      }
                      commit('setSnackbar',successMsg)
                      dispatch('getMyMeasurements')    
                    }
                    
                })
                .catch(error => {
                    state.errors = error.response.data.errors
                    if(error.response.status == 422){
                      let errorMsg = {
                        visible : true,
                        message : state.errors.date[0],
                        color : 'error',
                        icon : 'mdi-close-octagon'
                      }
                        
                        commit('setSnackbar',errorMsg);
                    }else if(error.response.status == 429){
                      let errorMsg = {
                        visible : true,
                        message : 'Error: status 409',
                        color : 'error',
                        icon : 'mdi-close-octagon'
                      }
                        
                        commit('setSnackbar',errorMsg);  
                           
                    }else if(error.response.status == 404){
                      let errorMsg = {
                        visible : true,
                        message : 'Ya existe con la misma fecha',
                        color : 'error',
                        icon : 'mdi-close-octagon'
                      }
                        
                        commit('setSnackbar',errorMsg);  
                           
                    }
                    
                });
      
  },//FIN
}
   
  // mutations
  const mutations = {
    setMyMeasurements(state,measurements){
      state.measurements = measurements
    },
    setSnackbar(state,msg){
      state.snackbar.visible = false
      state.snackbar.visible = msg.visible
      state.snackbar.message = msg.message
      state.snackbar.color = msg.color
      state.snackbar.icon = msg.icon
    },
    setD(state,d){
      state.dialog = false
      state.dialog = d
    },
    setMeasurementInfo(state,measurement){
      state.measurementInfo.dateInfo = measurement.date
      state.measurementInfo.weightInfo = measurement.weight
      state.measurementInfo.heightInfo = measurement.height
      state.measurementInfo.imcInfo = measurement.imc
      state.measurementInfo.fatInfo = measurement.fat
      state.measurementInfo.muscleInfo = measurement.muscle
      state.measurementInfo.caloricExpenditureInfo = measurement.caloricExpenditure
      state.measurementInfo.metabolicAgeInfo = measurement.metabolicAge
      state.measurementInfo.visceralFatInfo = measurement.visceralFat
      state.measurementInfo.chestInfo = measurement.chest
      state.measurementInfo.highAbdomenInfo = measurement.highAbdomen
      state.measurementInfo.lowerAbdomenInfo = measurement.lowerAbdomen
      state.measurementInfo.waistInfo = measurement.waist
      state.measurementInfo.buttocksInfo = measurement.buttocks
      state.measurementInfo.rightLegInfo = measurement.rightLeg
      state.measurementInfo.leftLegInfo = measurement.leftLeg
      state.measurementInfo.rightCalfInfo = measurement.rightCalf
      state.measurementInfo.leftCalfInfo = measurement.leftCalf
      state.measurementInfo.relaxedRightArmInfo = measurement.relaxedRightArm 
      state.measurementInfo.rightArmStressedInfo = measurement.rightArmStressed
      state.measurementInfo.relaxedLeftArmInfo = measurement.relaxedLeftArm
      state.measurementInfo.leftArmStressedInfo = measurement.leftArmStressed
    }
  }
  
  export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
  }