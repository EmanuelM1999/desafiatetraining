import axios from 'axios';

  //state
  const state = () => ({
    observations:[],
    snackbar:{
      visible: false,
      message:'',
      color:'',
      icon:''
    },
  })
  
  // getters
  const getters = {
    getObservations(state){
      return state.observations
    },
    getSnackbar(state){
      return state.snackbar
    }
  }
  
  // actions
  const actions = {
    getAllAuthObservations ({ commit }) {
        axios.get('/observation')
        .then(response => {                     
          commit('setObservations',response.data);                                    
        })
        .catch(function (error) {
             console.log(error);
        });
    },//FIN
    createAuthObservation({commit,dispatch,state},data){
        axios.post('/observation', {
            class_date: data.class_date,
            description: data.description,
          
        })
        .then(response => {
            console.log(response.status);
            if(response.status == 201){
              let successMsg = {
                visible : true,
                message : 'Agregada con exito',
                color : 'success',
                icon : 'mdi-checkbox-marked-circle'
              }
          
              commit('setSnackbar',successMsg);
              dispatch('getAllAuthObservations')          
            }                            
        })
        .catch(error => {
                if(error.response.status == 422){
                  let errorMsg = {
                    visible : true,
                    message : 'Todos los campos obligatorios',
                    color : 'error',
                    icon : 'mdi-close-octagon'
                  }
                    
                    commit('setSnackbar',errorMsg);
                }else if(error.response.status == 429){
                    
                }
                
            });


      //FIN
  }
}
   
  // mutations
  const mutations = {
    setObservations(state,observations){
      state.observations = []
      state.observations = observations
    },
    setSnackbar(state, msg){
      state.snackbar.visible = false
      state.snackbar.visible = msg.visible
      state.snackbar.message = msg.message
      state.snackbar.color = msg.color
      state.snackbar.icon = msg.icon
    },
  }
  
  export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
  }