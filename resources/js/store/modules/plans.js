import axios from 'axios';

  //state
  const state = () => ({
    all: [],
    editedPlan:{
      id:'',
      name:'',
      price:'',
      wod:'',
      wod_online:''
    },
    planD:{
      id:'',
      name:'',
      price:'',
      wod:'',
      wod_online:''
    },
    classes:[],
    errors:[],
    snackbar:{
      visible: false,
      message:'',
      color:'',
      icon:''
    },
    snackbarE:{
      visible: false,
      message:'',
      color:'',
      icon:''
    },
    snackbarEd:{
      visible: false,
      message:'',
      color:'',
      icon:''
    },
    snackbarD:{
      visible: false,
      message:'',
      color:'',
      icon:''
    },
  })
  
  // getters
  const getters = {
    getAll(state){
      return state.all
    },
    getEditedPlan(state){
      return state.editedPlan
    },
    getEditedClasses(state){
      return state.classes
    },
    getPlanD(state){
      return state.planD
    },
    getDialog(state){
     return state.dialog
    },
    getSnackbar(state){
       return state.snackbar
     },
    getSnackbarE(state){
      return state.snackbarE
    },
    getSnackbarEd(state){
      return state.snackbarEd
    },
    getSnackbarD(state){
      return state.snackbarD
    }
  }
  
  // actions
  const actions = {
    getAllPlans ({ commit }) {
        axios.get('/plans')
          .then(response => {                     
              console.log(response.data.name);
              commit('setPlans',response.data);
                                                  
          })
          .catch(function (error) {
              console.log(error);
          });
    },//FIN
    createPlan({commit,dispatch,state},data){
      axios.post('/plans', {
        name: data.name,  
        price: data.price,
        wod: data.wod,
        wod_online: data.wod_online                    
      })
      .then(response => { 
          if(response.status == 201){
            let successMsg = {
              visible : true,
              message : 'Creación exitosa',
              color : 'success',
              icon : 'mdi-checkbox-marked-circle'
            }
              
              commit('setSnackbar',successMsg);
              dispatch('getAllPlans')            
          }      
      })
      .catch(error => {
          if(error.response.status == 422){
              state.errors = error.response.data.errors
              let errorMsg = {
                visible : true,
                message : state.errors.name[0],
                color : 'error',
                icon : 'mdi-close-octagon'
              }
                
                commit('setSnackbar',errorMsg);
          }
          
      });
    },//FIN
    updatePlan({commit, dispatch, state},data){
      axios.put(`/plans/${data.id}`, {
        id: data.id,
        name: data.name,  
        price: data.price,
        wod: data.wod,
        wod_online: data.wod_online                    
      })
      .then(response => {
          
        console.log(response.status); 
        if(response.status == 200){
          let successMsg = {
            visible : true,
            message : 'Actualizado con exito',
            color : 'success',
            icon : 'mdi-checkbox-marked-circle'
          }
      
            commit('setSnackbarEd',successMsg);
            dispatch('getAllPlans')
        }      
      })
      .catch(error => {
          if(error.response.status == 422){
            state.errors = error.response.data.errors
            let errorMsg = {
              visible : true,
              message : state.errors.name[0],
              color : 'error',
              icon : 'mdi-close-octagon'
            }
              
              commit('setSnackbarEd',errorMsg);
          }
          
      });
    },//FIN
    deletePlan({commit, dispatch, state},data){
        axios.delete(`/plans/${data.id}`)
          .then(response => {
          
        console.log(response.status); 
        if(response.status == 200){
          let successMsg = {
            visible : true,
            message : 'Eliminado con exito',
            color : 'success',
            icon : 'mdi-checkbox-marked-circle'
          }
            commit('setSnackbarD',successMsg);
            dispatch('getAllPlans')
        }      
      })
      .catch(error => {
          if(error.response.status == 422){
            state.errors = error.response.data.errors
            let errorMsg = {
              visible : true,
              message : state.errors.name[0],
              color : 'error',
              icon : 'mdi-close-octagon'
            }
              
              commit('setSnackbarD',errorMsg);
          }
          
      });
    },
  }
   
  // mutations
  const mutations = {
    setPlans (state, plans) {
        state.all = plans
    },
    setPlan(state, plan){
      state.editedPlan.id = plan.id
      state.editedPlan.name = plan.name
      state.editedPlan.price = plan.price
      state.editedPlan.wod = plan.wod
      state.editedPlan.wod_online = plan.wod_online
    },
    setClasses(state,classesE){
      state.classes = classesE 
    },
    setPlanD(state, plan){
      state.planD.id = plan.id
      state.planD.name = plan.name
      state.planD.price = plan.price
      state.planD.wod = plan.wod
      state.planD.wod_online = plan.wod_online
    },
    setSnackbar(state, msg){
      state.snackbar.visible = msg.visible
      state.snackbar.message = msg.message
      state.snackbar.color = msg.color
      state.snackbar.icon = msg.icon
    },
    setSnackbarE(state, msg){
      state.snackbarE.visible = msg.visible
      state.snackbarE.message = msg.message
      state.snackbarE.color = msg.color
      state.snackbarE.icon = msg.icon
    },
    setSnackbarEd(state, msg){
      state.snackbarEd.visible = msg.visible
      state.snackbarEd.message = msg.message
      state.snackbarEd.color = msg.color
      state.snackbarEd.icon = msg.icon
    },
    setSnackbarD(state, msg){
      state.snackbarD.visible = false
      state.snackbarD.visible = msg.visible
      state.snackbarD.message = msg.message
      state.snackbarD.color = msg.color
      state.snackbarD.icon = msg.icon
    }
  }
  
  export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
  }