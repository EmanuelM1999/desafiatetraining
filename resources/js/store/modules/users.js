const state = () => ({
    users:[],
    plans:[],
    observations:[],
    measurements:[],
    payments:[],
    userInfo:{},
    userUpdated:{},
    userRolesUpdated:[],
    snackbarC:{
        visible: false,
        message:'',
        color:'',
        icon:''
    },
    snackbarU:{
        visible: false,
        message:'',
        color:'',
        icon:''
    },
    errors:[]
})

const getters = {
    getPlans(state){
        return state.plans
    },
    getUsers(state){
        return state.users
    },
    getUserInfoG(state){
        return state.userInfo
    },
    getObservations(state){
        return state.observations
    },
    getMeasurements(state){
        return state.measurements
    },
    getUserUpdated(state){
        return state.userUpdated
    },
    getUserRolesUpdated(state){
        return state.userRolesUpdated
    },
    getUserPayments(state){
        return state.payments
    },
    getSnackbarC(state){
        return state.snackbarC
    },
    getSnackbarU(state){
        return state.snackbarU
    }
}

const actions = {
    getAllPlans({commit}){
        axios.get('/plans')
          .then(response => {                     
              commit('setPlans',response.data);
                                                  
          })
          .catch(function (error) {
              console.log(error);
          });
    },
    getAllUsers({commit}){
        axios.get('/user')
        .then(response => {                     
            
            commit('setUsers',response.data);
                                               
        })
        .catch(function (error) {
             console.log(error);
        });
    },
    createUser({commit, dispatch, state},data){
        axios.post('/user', {
            email: data.email,
            administrador: data.administrador,
            coach: data.coach,
            atleta: data.atleta,
            plan_id: data.plan_id,
            first_login: data.first_login
        })
        .then(response => {
            
            console.log(response.status); 
            if(response.status == 201){
                let successMsg = {
                    visible : true,
                    message : 'Usuario creado con éxito',
                    color : 'success',
                    icon : 'mdi-checkbox-marked-circle'
                  }
                    
                    commit('setSnackbar',successMsg);
                    dispatch('getAllUsers')           
            } 
        })
        .catch(error => {
            if(error.response.status == 422){
                console.log(error.response.status); 
                state.errors = error.response.data.errors
                let errorMsg = {
                  visible : true,
                  message : state.errors.email[0],
                  color : 'error',
                  icon : 'mdi-close-octagon'
                }
                  
                  commit('setSnackbar',errorMsg);                
            }else if(error.response.status == 429){
                console.log(error.response.status); 
                state.errors = error.response.data.errors
                let errorMsg = {
                  visible : true,
                  message : 'Error: status 429',
                  color : 'error',
                  icon : 'mdi-close-octagon'
                }
                  
                  commit('setSnackbar',errorMsg);
            }
             
        });
    },
    updateUser({commit, dispatch, state},data){
        axios.post('/update-rol', {
            id: data.id,
            administrador: data.administrador,
            coach: data.coach,
            atleta: data.atleta              
        })
        .then(response => {
            if(response.status == 200){
                let successMsg = {
                    visible : true,
                    message : 'Usuario actualizado con éxito',
                    color : 'success',
                    icon : 'mdi-checkbox-marked-circle'
                }
                    
                commit('setSnackbarU',successMsg);
                dispatch('getAllUsers')
            } 
        }).catch(error => {
            if(error.response.status == 422){
                state.errors = error.response.data.errors
                let errorMsg = {
                visible : true,
                message : state.errors.email[0],
                color : 'error',
                icon : 'mdi-close-octagon'
                }
                
                commit('setSnackbarU',errorMsg);

            }else if(error.response.status == 429){
                state.errors = error.response.data.errors
                let errorMsg = {
                visible : true,
                message : state.errors,
                color : 'error',
                icon : 'mdi-close-octagon'
                }
                
                commit('setSnackbarU',errorMsg);
            }
        
        });  
    },
    getUserInfo({commit,state},data){
        console.log('id seleccionado :' + data);
        
        axios.post('/show-info-by-id', {
            id: data            
        })
        .then(response => {
            commit('setUserInfo',response.data[0])
        })
    },
    getUserObservations({commit},data){
        axios.post('/list-observations', {
            user_id: data
        })
        .then(response => {
            commit('setObservations',response.data)
        })
    },
    getUserMeasurements({commit},data){
        axios.post("/list-measurements", {
            user_id: data
        })
        .then(response => {
            commit('setMeasurements',response.data)
        });
    },
    getAllUserPayments({commit},data){
        axios.post('/list-payments', {
          user_id: data            
        })
        .then(response => {
          commit('setUserPayments',response.data)          
        }) 
    },
}

const mutations = {
    setPlans(state,plans){
        state.plans = plans
    },
    setUsers(state,users){
        state.users = users
    },
    setUserInfo(state,info){
        state.userInfo = info
    },
    setUserUpdated(state,user){
        state.userUpdated = user
    },
    setUserRolesUpdated(state,roles){
        state.userRolesUpdated = []
        state.userRolesUpdated = roles 
    },
    setObservations(state,observations){
        state.observations = observations
    },
    setMeasurements(state,measurements){
        state.measurements = measurements
    },
    setUserPayments(state,payments){
        state.payments = []
        state.payments = payments
    },
    setSnackbar(state, msg){
        state.snackbarC.visible = false
        state.snackbarC.visible = msg.visible
        state.snackbarC.message = msg.message
        state.snackbarC.color = msg.color
        state.snackbarC.icon = msg.icon
    },
    setSnackbarU(state, msg){
        state.snackbarU.visible = false
        state.snackbarU.visible = msg.visible
        state.snackbarU.message = msg.message
        state.snackbarU.color = msg.color
        state.snackbarU.icon = msg.icon
    },
    
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}