import axios from 'axios';

  //state
  const state = () => ({
    userAuthPayments:[],
    payments:[],
    plans:[],
    payment:{
      id:0
    },
    snackbarC:{
      visible: false,
      message:'',
      color:'',
      icon:'',
      state:false
    },
  })
  
  // getters
  const getters = {
    getUserAuthPayments(state){
      return state.userAuthPayments
    },
    getSnackbarC(state){
      return state.snackbarC
    },
    getPlans(state){
      return state.plans
    }
  }
  
  // actions
  const actions = {
    getAllUserAuthPayments ({ commit }) {
      axios.get('/payments')
        .then(response => {                     
          commit('setUserAuthPayments',response.data)
        })
        .catch(function (error) {
          console.log(error);
        });
    },//FIN
    getAllPlans({commit}){
      axios.get('/plans')
      .then(response => {   
          console.log('Yo cargué los planes');                  
          commit('setPlans',response.data)                                    
      })
      .catch(function (error) {
        console.log(error);
      });

    },
    createPayment({commit},data){
      axios.post('/payments', {
        plan_id: data.plan_id,
        paymentDate: data.paymentDate,
        expirationDate: data.expirationDate,
        typePlan: data.typePlan,
        planValue: data.planValue,
        discountPayment: data.discountPayment,
        total: data.total,
        user_id: data.user_id         
      })
      .then(response => {
        if(response.status == 201){
          console.log('Registro exitoso');
          let successMsg = {
            visible : true,
            message : 'Pago registrado con éxito',
            color : 'success',
            icon : 'mdi-checkbox-marked-circle',
            state: true
        }
            
        commit('setSnackbarC',successMsg);
        }   
      })
    },
    downloadpdf({commit,state},data) {
      commit('setPaymentId',data)
      axios.get('/generate-PDF/' + state.payment.id, {responseType: 'arraybuffer'})
        .then(response => {                     
          let blob = new Blob([response.data], { type: 'application/pdf' })
          let link = document.createElement('a')
          link.href = window.URL.createObjectURL(blob)
          link.download = `ReciboNo${state.payment.id}.pdf`
          link.click()                            
        }) 
    },
}
   
  // mutations
  const mutations = {
    setUserAuthPayments(state,payments){
      state.userAuthPayments = payments
    },
    setPlans(state,plans){
      state.plans = plans
    },
    setPaymentId(state,id){
      state.payment.id = id
    },
    setSnackbarC(state, msg){
      state.snackbarC.visible = false
      state.snackbarC.state = false
      state.snackbarC.visible = msg.visible
      state.snackbarC.message = msg.message
      state.snackbarC.color = msg.color
      state.snackbarC.icon = msg.icon
      state.snackbarC.state = msg.state
  },
  }
  
  export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
  }