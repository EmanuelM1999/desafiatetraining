const state = () => ({

})

const getters = {

}

const actions = {
    getCoaches({ commit, dispatch, state }, data) {
        axios.get('/query-coaches')
            .then(response => {
                this.coaches = response.data;
            })
            .catch(function (error) {
                console.log(error);
            });
    },
    addTemplate({ commit, dispatch, state }, data) {
        axios.post('/class-template', { name_template: this.template.name })
            .then(response => {
                if (response.status == 201) {
                    this.flashMessage.success({
                        status: 'success',
                        message: 'Creación exitosa',
                        time: 2000,
                    });
                    this.templates = []
                    this.getTemplates();
                    this.errorEmptySpaces = false
                    this.template.name = ''
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    },
    getTemplates({ commit, dispatch, state }, data) {
        axios.get('/class-template')
            .then(response => {
                this.templates = response.data;
            })
            .catch(function (error) {
                console.log(error);
            });
    },
    deleteTemplate({ commit, dispatch, state }, data) {
        axios.delete(`/class-template/${this.template.idTemplate}`)
            .then(response => {
                this.flashMessage.success({
                    status: 'success',
                    message: 'Eliminada con éxito',
                    time: 1500,
                });
                this.templates = []
                this.getTemplates();
                $('#deleteTemplateModal .close').click()
            })
            .catch(function (error) {
                console.log(error);
            });
    },
    getClasses({ commit, dispatch, state }, data) {
        axios.post('/query-classes', {
            id: this.template.idTemplate,
        })
            .then(response => {

                if (response.status == 200) {
                    this.monday = [];
                    this.tuesday = [];
                    this.thursday = [];
                    this.wednesday = [];
                    this.sunday = [];
                    this.saturday = [];
                    this.friday = [];

                    for (let i = 0; i < response.data.length; i++) {
                        if (response.data[i].day == 'lunes') {
                            this.monday.push(response.data[i]);
                        } else if (response.data[i].day == 'martes') {
                            console.log("adssad " + response.data[i]);
                            this.tuesday.push(response.data[i]);
                        } else if (response.data[i].day == 'miercoles') {
                            this.wednesday.push(response.data[i]);
                        } else if (response.data[i].day == 'jueves') {
                            this.thursday.push(response.data[i]);
                        } else if (response.data[i].day == 'viernes') {
                            this.friday.push(response.data[i]);
                        } else if (response.data[i].day == 'sabado') {
                            this.saturday.push(response.data[i]);
                        } else if (response.data[i].day == 'domingo') {
                            this.sunday.push(response.data[i]);
                        }

                    }
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    },
    addClass({ commit, dispatch, state }, data) {
        for (let index = 0; index < this.daysSelectedC.length; index++) {

            axios.post('/class-schedule', {
                id_template: this.template.idTemplate,
                wod: wodValue,
                wod_online: wod_onlineValue,
                places: this.lessonC.placesC,
                day: this.daysSelectedC[index],
                hour: this.lessonC.hourC,
                coach: this.coachesSelectedC,
            })
                .then(response => {
                    console.log(response.status);
                    if (response.status == 200) {
                        this.flashMessage.success({
                            status: 'success',
                            message: 'Creación exitosa',
                            time: 2000,
                        });
                        this.lessonC.typeC = ''
                        this.lessonC.hourC = ''
                        this.daysSelectedC = []
                        this.coachesSelectedC = ''
                        this.lessonC.placesC = ''
                        this.errorClassType = false
                        this.errorHour = false
                        this.errorDays = false
                        this.errorCoaches = false
                        this.errorPlaces = false
                        this.getClasses();
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        }
    },
    editClass({ commit, dispatch, state }, data){
        axios.put(`/class-schedule/${this.lesson.id}`,{                
            wod: wodValue,
            wod_online: wod_onlineValue,
            places: this.lesson.places,
            day: this.daysSelected,
            hour: this.lesson.hour,
            coach: this.coachesSelected,
        })
            .then(response => {
                console.log(response.status);
                if (response.status == 200) {
                    this.flashMessage.success({
                        status: 'success',
                        message: 'Actualiazción exitosa',
                        time: 2000,
                    });
                    this.classes= [];                                      
                    this.getClasses();
                    this.lesson.type = ''  
                    this.lesson.hour = ''
                    this.daysSelected = ''
                    this.coachesSelected = ''
                    this.lesson.places = ''
                    this.errorClassType = false
                    this.errorHour = false
                    this.errorDays = false
                    this.errorCoaches = false
                    this.errorPlaces = false       
                }
                
            })
            .catch(function (error) {
                console.log(error);
            });
    },
    deleteClass({ commit, dispatch, state }, data){
        axios.delete(`/class-schedule/${this.lesson.id}`)
                .then(response => {
                    this.flashMessage.success({
                        status: 'success',
                        message: 'Eliminada con éxito',
                        time: 1500,
                    });
                    this.monday = [];
                        this.tuesday = [];
                        this.thursday = [];
                        this.wednesday = [];
                        this.sunday = [];
                        this.saturday = [];
                        this.friday = [];
                    this.getClasses();
                    $("#deleteClassM .close").click()                                                                             
                })
                .catch(function (error) {
                     console.log(error);
                }); 
    },
}

const mutations = {

}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}