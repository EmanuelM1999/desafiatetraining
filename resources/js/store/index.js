import Vue from 'vue'
import Vuex from 'vuex'
import freeClass from './modules/freeClass'
import plans from './modules/plans'
import users from './modules/users'
import mybody from './modules/mybody'
import observations from './modules/observations'
import payments from './modules/payments'
import classtemplate from './modules/classtemplate'
import reports from './modules/reports'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    freeClass,
    plans,
    users,
    mybody,
    observations,
    payments,
    classtemplate,
    reports,
  },
}) 