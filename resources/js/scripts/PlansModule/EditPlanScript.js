import datatables from 'datatables';

export default {
    created() {
                         
    },
    data() {
        return {
            //Crear planes
            plansC:[],
            planC:{name: '',price: '',wod:'',wod_online:'' },
            checkClassesC:[],
            errorCheck:false, errorName: false, errorPrice: false,
            editActive: false,

            /////
            plans:[],
            plan:{id: null, name: '',price: '',wod:'',wod_online:'' },
            checkClasses:[],
            classes:[
                {value: 'wod', text: 'Wod'},
                {value: 'wod online', text: 'Wod online'}
            ],
            errors:[],
            errorCheck: false, errorName: false, errorPrice: false,
            editActive: false
        }
    },
    methods:{
        //Crear planes
        create(){
            this.planC.wod = false;  
            this.planC.wod_online = false;
            this.errorName = false
            this.errorPrice = false
            this.errorCheck = false

            if(this.planC.name == ''){
                this.errorName = true
            }
            if(this.planC.price == ''){
                this.errorPrice = true
            }
            if(this.checkClassesC.length == 0){
                this.errorCheck = true
            }
            if(this.planC.name != '' && this.planC.price != '' && this.checkClassesC.length != 0){
                for (let i = 0; i < this.checkClassesC.length; i++) {
                        if (this.checkClassesC[i]=='wod') {                                
                            this.planC.wod = true;                    
                        }else if (this.checkClassesC[i]=='wod online') {
                            this.planC.wod_online = true;
                        }                                                                                  
                }   
  
                axios.post('/plans', {
                    name: this.planC.name,  
                    price: this.planC.price,
                    wod: this.planC.wod,
                    wod_online: this.planC.wod_online                    
                })
                .then(response => {
                    
                    console.log(response.status); 
                    if(response.status == 201){
                        this.flashMessage.success({
                            message: 'Creación exitosa',
                            time:2000,
                        });
                    this.planC.name = '';
                    this.planC.price = '';
                    this.checkClassesC.splice(0,this.checkClassesC.length);
                    this.errorName = false
                    this.errorPrice = false
                    this.errorCheck = false
                    this.errors = []
                    //Carga los planes mediante AJAX
                    this.destroyDatatable();
                    this.loadPlans();
                    }      
                })
                .catch(error => {
                    if(error.response.status == 422){
                         this.errors = error.response.data.errors
                             this.flashMessage.error({
                                status: 'error',
                             message: 'Error en la creación del plan',
                             time: 2000,
                         });
                     }
                     
                });                  
                
            }
               
        },
        emptyFormCreate(){
            this.planC.name = '';
            this.planC.price = '';
            this.checkClassesC.splice(0,this.checkClassesC.length);
            this.errorName = false
            this.errorPrice = false
            this.errorCheck = false
            this.errors = [] 
        },
            
        editForm(item){
            
            $('#class0').prop('checked',false);
            $('#class1').prop('checked',false);
            this.plan.id = item.id
            this.plan.name = item.name
            this.plan.price = item.price
            this.plan.wod = item.wod
            this.plan.wod_online = item.wod_online
            this.checkClasses = [];
            if(item.wod === 1){ 
                $('#class0').prop('checked',true);
                this.checkClasses.push('wod');

            }
            if(item.wod_online === 1){               
                $('#class1').prop('checked',true);
                this.checkClasses.push('wod online');
            }
           
          
           
        },



        editPlan(item){
       
            this.plan.wod = false;
            this.plan.wod_online = false;
            this.errorName = false
            this.errorPrice = false
            this.errorCheck = false

            if(this.plan.name == ''){
                this.errorName = true
            }
            if(this.plan.price == ''){
                this.errorPrice = true
            }
            if(this.checkClasses.length == 0){
                this.errorCheck = true
            }
            if(this.plan.name != '' && this.plan.price != '' && this.checkClasses.length != 0){
                for (let i = 0; i < this.checkClasses.length; i++) {
                        if (this.checkClasses[i]=='wod') {                                
                            this.plan.wod = true;                    
                        }else if (this.checkClasses[i]=='wod online') {
                            this.plan.wod_online = true;
                        }                                                                                  
                }
 
                
                axios.put(`/plans/${this.plan.id}`, {
                    id: this.plan.id,
                    name: this.plan.name,  
                    price: this.plan.price,
                    wod: this.plan.wod,
                    wod_online: this.plan.wod_online                    
                })
                .then(response => {
                    
                    console.log(response.status); 
                    if(response.status == 200){
                        this.flashMessage.success({
                            message: 'Actualización exitosa',
                            time:2000,
                        });
                        this.plan.name = '';
                        this.plan.price = '';
                        this.checkClasses.splice(0,this.checkClasses.length);
                        this.errorName = false
                        this.errorPrice = false
                        this.errorCheck = false
                        this.errors = []
                        //Carga los planes mediante AJAX
                        this.destroyDatatable();
                        this.loadPlans();
                    }

                          
                })
                .catch(error => {
                    if(error.response.status == 422){
                        this.errors = error.response.data.errors
                            this.flashMessage.error({
                            status: 'error',
                            message: 'Error en la actualización del plan',
                            time: 2000,
                        });
                    }
                     
                });                  
              
            }
               
        },


        idDeletePlan(item){
            this.plan.id = item.id
            
        },

        deletePlan(){
           
           axios.delete(`/plans/${this.plan.id}`)
             .then(response =>{
                    this.flashMessage.success({
                        status: 'success',
                        message: 'Eliminado con éxito',
                        time: 1500,
                    }); 
                    this.destroyDatatable();
                    this.loadPlans();
                    $("#deletePlanModal .close").click()
                     
          }).catch(function (error) {
                     console.log(error);
                });  

        
        
        },

        emptyErrors(){
            this.plan.name = '';
            this.plan.price = '';
            this.checkClasses.splice(0,this.checkClasses.length);
            this.errorName = false
            this.errorPrice = false
            this.errorCheck = false
            this.errors = [] 
        },

        msgSuccessfull($event){
            
        }
    }
}