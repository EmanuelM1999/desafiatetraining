/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router'
import FlashMessage from '@smartweb/vue-flash-message';
//import vSelect from 'vue-select'
import store from './store'
import VuePaginate from 'vue-paginate'
// import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
// import 'bootstrap/dist/css/bootstrap.css'
// import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueChartjs from 'vue-chartjs';

import vuetify from './plugins/vuetify';


//Vue.component('v-select', vSelect)
Vue.use(FlashMessage);
Vue.use(VueRouter)
Vue.use(VuePaginate)
Vue.use(VueChartjs);
// Install BootstrapVue
// Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
// Vue.use(IconsPlugin)

const routes = [   
    { path: '/home-plans', component: require('./components/HomePage/homePlansComponent.vue').default},
    { path: '/home-coach', component:  require('./components/HomePage/homeCoachComponent.vue').default},
    { path: '/dashboard', component:  require('./components/dashboard.vue').default},
    { path: '/login-firs-time', component:  require('./components/LoginFirstTimeComponent.vue').default},
    { path: '/plan-module', component:  require('./components/PlansModule/PlansModuleComponent.vue').default},
    { path: '/create-plan', component:  require('./components/PlansModule/CreatePlanComponent.vue').default},
    { path: '/plan-list', name: 'list',component:  require('./components/PlansModule/ListPlanComponent.vue').default},
    { path: '/users-module', component:  require('./components/UsersModule/Users/UsersModuleComponent.vue').default},
    { path: '/edit-user', component:  require('./components/UsersModule/Users/EditUserComponent.vue').default},
    { path: '/user-observation', component:  require('./components/UsersModule/UserAuth/UserAuthObservationsComponent.vue').default},
    { path: '/my-body', component:  require('./components/UsersModule/UserAuth/MyBodyComponent.vue').default},
    { path: '/list-payments', component:  require('./components/UsersModule/UserAuth/ListPaymentsComponent.vue').default},
    { path: '/weekly-classes', component:  require('./components/ClassesModule/ClassesTemplates/WeeklyClassesComponent.vue').default},
    { path: '/reserve-class', component:  require('./components/ClassesModule/ReserveClasses/ReserveClassComponent.vue').default},
    { path: '/reports-module', component:  require('./components/ReportsModule/ReportsModuleComponent.vue').default},
    { path: '/my-profile', component:  require('./components/UsersModule/UserAuth/MyProfileComponent.vue').default},
  ]

  const router = new VueRouter({
    routes // short for `routes: routes`
  })
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */ 

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

//COMPONENTES

//Homepage
Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('homepage-component', require('./components/HomePage/homePageComponent.vue').default);
Vue.component('homeplans-component', require('./components/HomePage/homePlansComponent.vue').default);
Vue.component('homecoach-component', require('./components/HomePage/homeCoachComponent.vue').default);
Vue.component('homepagebox-component', require('./components/HomePage/homePageBoxComponent.vue').default);
Vue.component('freeclass-component', require('./components/HomePage/freeClassComponent.vue').default);
Vue.component('login-component', require('./components/LoginComponent.vue').default);

// Users
Vue.component('usermodule-component', require('./components/UsersModule/Users/UsersModuleComponent.vue').default);
Vue.component('createuser-component', require('./components/UsersModule/Users/CreateUserComponent.vue').default);
Vue.component('userinfo-component', require('./components/UsersModule/Users/UserInfoComponent.vue').default);
Vue.component('theirbody-component', require('./components/UsersModule/Users/TheirBodyComponent.vue').default);
Vue.component('contactuser-component', require('./components/UsersModule/contactUserComponent.vue').default);
Vue.component('edituser-component', require('./components/UsersModule/Users/EditUserComponent.vue').default);
Vue.component('userlist-component', require('./components/UsersModule/Users/UserListComponent.vue').default);
Vue.component('renewpayment-component', require('./components/UsersModule/Users/RenewPaymentComponent.vue').default);

// User Auth
Vue.component('listuserauth-observations-component', require('./components/UsersModule/UserAuth/ListUserAuthObservationsComponent.vue').default);
Vue.component('userobservations-component', require('./components/UsersModule/Users/UserObservationsComponent.vue').default);
Vue.component('createobservation-component', require('./components/UsersModule/UserAuth/CreateObservationComponent.vue').default);
Vue.component('registermeasurements-component', require('./components/UsersModule/UserAuth/RegisterMeasurementsComponent.vue').default);
Vue.component('mymeasurementsinfo-component', require('./components/UsersModule/UserAuth/MyMeasurementsInfoComponent.vue').default);

//Plans
Vue.component('plancreation-component', require('./components/PlansModule/PlansModuleComponent.vue').default);
Vue.component('listplan-component', require('./components/PlansModule/ListPlanComponent.vue').default);
Vue.component('createplan-component', require('./components/PlansModule/CreatePlanComponent.vue').default);
Vue.component('editplan-component', require('./components/PlansModule/EditPlanComponent.vue').default);
Vue.component('deleteplan-component', require('./components/PlansModule/DeletePlanComponent.vue').default);

//Templates
Vue.component('listtemplate-component', require('./components/ClassesModule/ClassesTemplates/ListClassTemplates.vue').default);
Vue.component('cretatetemplate-component', require('./components/ClassesModule/ClassesTemplates/CreateTemplateComponent.vue').default);
Vue.component('deletetemplate-component', require('./components/ClassesModule/ClassesTemplates/DeleteTemplateComponent.vue').default);
Vue.component('edittemplate-component', require('./components/ClassesModule/ClassesTemplates/EditTemplateComponent.vue').default);

// Sugerir nombre o en dónde ponerlo
Vue.component('dashboard-component', require('./components/dashboard.vue').default);
Vue.component('loginfirsttime-component', require('./components/LoginFirstTimeComponent.vue').default);

// Reports
Vue.component('report-ages', require('./components/ReportsModule/ReportAgesComponent.vue').default);
Vue.component('report-students-by-plan', require('./components/ReportsModule/ReportStudentsByPlanComponent.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */ 

const app = new Vue({
    el: '#app',
    vuetify,
    router,
    store,     
});

