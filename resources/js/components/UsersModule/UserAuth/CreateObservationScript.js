import { mapActions, mapGetters, mapMutations } from 'vuex'

export default {
    created() {
        this.getCurrentDate()
    },
    data() {
        return {
            date:'',
            menu: false,
            valid: false,
            alert:{
                visible: false,
                message:'',
                color:'',
                icon:''
            },
            observation: {class_date: '', description:''},
            dateRules: [
                v => !!v || 'Este es un campo obligatorio',
            ],
            descriptionRules: [
                v => !!v || 'Este es un campo obligatorio',
            ],
            currentDate: '', month:'',day:'',year:'',fullDate:''
        }
    },
    methods: {
        ...mapActions('observations',['createAuthObservation']),
        ...mapMutations('observations',['setSnackbar']),
    
        validate(){
            if (this.$refs.form.validate()) {
                this.createAuthObservation(this.observation)
                this.reset()
            }else{
                this.alert.visible = true
                this.alert.message = 'Todos los campos son obligatorios'
                this.alert.color = 'error'
                this.alert.icon = 'mdi-close-octagon'
                this.setSnackbar(this.alert)
            }
        },
        reset () {
            this.$refs.form.reset()
        },
        getCurrentDate(){
            this.currentDate = new Date()
            this.month = this.currentDate.getMonth() +1;
            this.day = this.currentDate.getDate();
            this.year = this.currentDate.getFullYear();
            if(this.day<10){
                this.day='0'+this.day
            }
            if(this.month<10){
                this.month='0'+this.month
            }
            this.fullDate = this.year + "-" + this.month + "-" + this.day
            this.observation.class_date = this.year + "-" + this.month + "-" + this.day
        }
    },
    computed: {
        ...mapGetters('observations',['getSnackbar']),
        snackbar(){
            this.alert.visible = this.getSnackbar.visible
            this.alert.message = this.getSnackbar.message
            this.alert.color = this.getSnackbar.color
            this.alert.icon = this.getSnackbar.icon
            return this.alert
        }
    },
}