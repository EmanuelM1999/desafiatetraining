import {mapActions,mapGetters} from 'vuex'

export default {
    created() {
        this.getAllUserAuthPayments();
    },
    data() {
        return {
        //Para trabajar los pagos
            payments:[],
            payment:{id: '',typePlan:'',paymentDate:'',expirationDate:'',discountPayment: '', planValue: '',total: '', user_id: ''},    
            perPage:5,
            headers: [
                { text: 'Recibo No.', align: 'start', value: 'id'},
                { text: 'Fecha de pago', align: 'start', value: 'paymentDate'},
                { text: 'Fecha de vencimiento', align: 'start', value: 'expirationDate'},
                { text: 'Plan', align: 'start', value: 'typePlan'},
                { text: 'Valor', align: 'start', value: 'planValue'},
                { text: 'Descuento', align: 'start', value: 'discountPayment'},
                { text: 'Total', align: 'start', value: 'total'},
                { text: 'Acciones', value: 'actions', sortable: false },
                ],
            
            measurements:[],
        }
    },
    methods: {
        ...mapActions('payments',['getAllUserAuthPayments','downloadpdf']),
    },
    computed: {
        ...mapGetters('payments',['getUserAuthPayments'])
    },
}