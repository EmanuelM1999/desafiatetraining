$("#exampleModal").on("show.bs.modal", function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var recipient = button.data("whatever"); // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this);
    modal.find(".modal-title").text("New message to " + recipient);
    modal.find(".modal-body input").val(recipient);
  });
  
  export default {
    created() {
      this.getUserAuth();
    },
    data() {
      return {
        sexSelected: "",
        countrySelected: "",
        typeIdSelected: "",
        errors: [],
        errorSurname1: false,
        errorSurname2: false,
        sexes: [
          { value: "masculino", text: "Masculino" },
          { value: "femenino", text: "Femenino" },
          { value: "indefinido", text: "Prefiero no decir" },
        ],
        typesId: [
          { value: "CC", text: "Cédula de ciudadanía" },
          { value: "TI", text: "Tarjeta de identidad" },
          { value: "CE", text: "Tarjeta de extranjería" },
        ],
        confirmPassword:'',
        user: {
          id: '',
          name: '',
          surnames: '',
          photo: '',
          id_type: '',
          telephone: '',
          sex: '',
          age: '',
          date_of_birth: '',
          visible_date_of_birth: '',
          home_address: '',
          city: '',
          departament: '',
          country: '',
          weight: '',
          height: '',
          visible_weight: '',
          visible_height: '',
          visible_benchmark_body: '',
          name_contact_emergency: '',
          telephone_contact_emergency: '',
          email: '',
          administrador: '',
          coach: '',
          atleta: '',
          plan_id: '',
          password:'',
          first_login: '',
        },
        errorConfirmPassword:false,errorDifferentPasswords:false
      };
    },
    methods: {
      getUserAuth(){
              axios.get('/get-user-auth')
                  .then(response => {
                    console.log(response.data);                     
                      this.user = response.data;                                   
                  })
                  .catch(function (error) {
                       console.log(error);
                });
                
      },
      selectImage(e) {
        var file = new FileReader();
        file.readAsDataURL(e.target.files[0]);
        file.onload = (e) => {
          this.user.photo = e.target.result;
        };
      },
      updateUser() {     
              axios
                .put('/user/""', {
                  photo: this.user.photo,                
                  name: this.user.name,
                  surnames: this.user.surnames,                
                  telephone: this.user.telephone,
                  sex: this.user.sex,
                  age: this.user.age,
                  date_of_birth: this.user.date_of_birth,
                  home_address: this.user.home_address,
                  city: this.user.city,
                  departament: this.user.departament,
                  country: this.user.country,
                  weight: this.user.weight,
                  height: this.user.height,
                  name_contact_emergency: this.user.name_contact_emergency,
                  telephone_contact_emergency: this.user.telephone_contact_emergency,
                  first_login: true,
                  password: this.user.password,
                })
                .then((response) => {
                  console.log(response.status);
                  if (response.status == 200) {
                    this.flashMessage.success({
                      message: "Actualizado con exito",
                      time: 2000,
                    });                  
                  }
                })
                .catch((error) => {
                  
                  if (this.user.password != '' && this.confirmPassword != '') {
                    if (this.user.password != this.confirmPassword) {
                   this.errorDifferentPasswords = true  
                  }else{this.errorDifferentPasswords = false}  
                  }
                  
                    console.log(error.response.data.errors);
                    this.errors = error.response.data.errors;
                    this.flashMessage.error({
                      status: "error",
                      message: "Error en el registro",
                      time: 2000,
                    });
                  
                });
                 },
    },
  };