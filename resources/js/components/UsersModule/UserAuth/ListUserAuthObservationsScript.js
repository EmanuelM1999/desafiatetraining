import { mapActions, mapGetters, mapMutations } from 'vuex'

export default{
    data() {
        return {
            observations:[],
            total: 0,
            paginas: 0,
            porPagina: 6,
            page: 1
        }
    },
    methods: {
        ...mapActions('observations',['getAllAuthObservations'])
    },
    computed:{
        ...mapGetters('observations',['getObservations']),
        list(){
            this.observations = this.getObservations
            return this.observations
        }
    },
    created() {
      this.getAllAuthObservations()  
    },
}