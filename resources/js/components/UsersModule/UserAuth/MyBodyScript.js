import {mapMutations,mapGetters, mapActions} from 'vuex'

export default {
    created() {
        this.getMyMeasurements();
    },
    data() {
        return {
            perPage:5,
            headers: [
                { text: 'Fecha', align: 'start', value: 'date'},
                { text: 'Acciones', value: 'actions', sortable: false },
                ],
            
            measurements:[],
        }
    },
    methods: {
        ...mapActions('mybody',['getMyMeasurements']),
        ...mapMutations('mybody', ['setMeasurementInfo']),
        
    },
    computed: {
        ...mapGetters('mybody',['getmyMeasurements']),
        list(){
            this.measurements = this.getmyMeasurements
            return this.measurements
        }        
    },    
}