import { mapActions, mapGetters, mapMutations } from 'vuex'

export default {
    created() {
        this.getCurrentDate()
    },
    data() {
        return {
            dialog:false,
            valid:false,
            alert:{
                visible: false,
                message:'',
                color:'',
                icon:''
            },
            modalTitle: '',

            measurement:{date:'',weight:'', height:'', imc:'', fat:'', muscle:'', caloricExpenditure:'', 
            metabolicAge:'', visceralFat:'', chest:'', highAbdomen:'', lowerAbdomen:'', waist:'', buttocks:'', 
            rightLeg:'', leftLeg:'', rightCalf:'', leftCalf:'', relaxedRightArm:'', rightArmStressed:'', 
            relaxedLeftArm:'', leftArmStressed:''},

            
            measurements:[],
            visibleMyMeasurements:false, visibleFormMeasurements:false, visibleSecondList:false,
            visibleCompareMeasurements: true, visibleMeasurementsTable:true,
            currentDate: '', month:'',day:'',year:'',fullDate:'',
            dateRules: [
                v => !!v || 'Este es un campo obligatorio',
            ],
            weightRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido',
            ],
            heightRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido',
            ],
            imcRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido',
            ],
            fatRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido',
            ],
            muscleRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido',
            ],
            caloricExpenditureRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido',
            ],
            metabolicAgeRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido',
            ],
            visceralFatRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido',
            ],
            chestRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido',
            ],
            highAbdomenRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido',
            ],
            lowerAbdomenRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido',
            ],
            waistRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido',
            ],
            buttocksRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido',
            ],
            rightLegRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido',
            ],
            leftLegRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido',
            ],
            rightCalfRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido',
            ],
            leftCalfRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido',
            ],
            relaxedRightArmRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido',
            ],
            rightArmStressedRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido',
            ],
            relaxedLeftArmRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido',
            ],
            leftArmStressedRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido',
            ],
            measurementInfo:{dateInfo:'',weightInfo:'', heightInfo:'', imcInfo:'', fatInfo:'', muscleInfo:'', caloricExpenditureInfo:'', 
            metabolicAgeInfo:'', visceralFatInfo:'', chestInfo:'', highAbdomenInfo:'', lowerAbdomenInfo:'', waistInfo:'', buttocksInfo:'', 
            rightLegInfo:'', leftLegInfo:'', rightCalfInfo:'', leftCalfInfo:'', relaxedRightArmInfo:'', rightArmStressedInfo:'', 
            relaxedLeftArmInfo:'', leftArmStressedInfo:''},
            
        }
    },
    methods: {
        ...mapActions('mybody',['addMeasurementRecord']),
        ...mapMutations('mybody',['setSnackbar','setD']),
        validate(){
            if (this.$refs.form.validate()) {
                this.addMeasurementRecord(this.measurement)
                this.$refs.form.resetValidation()
            }else{
                this.alert.visible = true
                this.alert.message = 'Todos los campos son obligatorios'
                this.alert.color = 'error'
                this.alert.icon = 'mdi-close-octagon'
                this.setSnackbar(this.alert)
            }
        },
        reset(){
            this.$refs.form.reset()
        },
        getCurrentDate(){
            this.currentDate = new Date()
            this.month = this.currentDate.getMonth() +1;
            this.day = this.currentDate.getDate();
            this.year = this.currentDate.getFullYear();
            if(this.day<10){
                this.day='0'+this.day
            }
            if(this.month<10){
                this.month='0'+this.month
            }
            this.fullDate = this.year + "-" + this.month + "-" + this.day
            this.measurement.date = this.year + "-" + this.month + "-" + this.day
        },
    },
    watched: {
        getD(){
            this.dialog = this.getD
        }
    },
    computed: {
        ...mapGetters('mybody',['getSnackbar','getD'],),
        snackbar(){
            this.alert.visible = this.getSnackbar.visible
            this.alert.message = this.getSnackbar.message
            this.alert.color = this.getSnackbar.color
            this.alert.icon = this.getSnackbar.icon
            return this.alert
        },
    },    
}