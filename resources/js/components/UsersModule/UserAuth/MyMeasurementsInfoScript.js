import {mapGetters} from 'vuex'
export default {
    created() {
    },
    data() {
        return {
            measurementInfo:{dateInfo:'',weightInfo:'', heightInfo:'', imcInfo:'', fatInfo:'', muscleInfo:'', caloricExpenditureInfo:'', 
            metabolicAgeInfo:'', visceralFatInfo:'', chestInfo:'', highAbdomenInfo:'', lowerAbdomenInfo:'', waistInfo:'', buttocksInfo:'', 
            rightLegInfo:'', leftLegInfo:'', rightCalfInfo:'', leftCalfInfo:'', relaxedRightArmInfo:'', rightArmStressedInfo:'', 
            relaxedLeftArmInfo:'', leftArmStressedInfo:''},
        }
    },
    methods: {
        
    },
    computed: {
        ...mapGetters('mybody',['getMeasurementInfo']),
        getMeasurementInfoC(){
            this.measurementInfo.dateInfo = this.getMeasurementInfo.dateInfo
            this.measurementInfo.weightInfo = this.getMeasurementInfo.weightInfo
            this.measurementInfo.heightInfo = this.getMeasurementInfo.heightInfo
            this.measurementInfo.imcInfo = this.getMeasurementInfo.imcInfo
            this.measurementInfo.fatInfo = this.getMeasurementInfo.fatInfo
            this.measurementInfo.muscleInfo = this.getMeasurementInfo.muscleInfo
            this.measurementInfo.caloricExpenditureInfo = this.getMeasurementInfo.caloricExpenditureInfo
            this.measurementInfo.metabolicAgeInfo = this.getMeasurementInfo.metabolicAgeInfo
            this.measurementInfo.visceralFatInfo = this.getMeasurementInfo.visceralFatInfo
            this.measurementInfo.chestInfo = this.getMeasurementInfo.chestInfo
            this.measurementInfo.highAbdomenInfo = this.getMeasurementInfo.highAbdomenInfo
            this.measurementInfo.lowerAbdomenInfo = this.getMeasurementInfo.lowerAbdomenInfo
            this.measurementInfo.waistInfo = this.getMeasurementInfo.waistInfo
            this.measurementInfo.buttocksInfo = this.getMeasurementInfo.buttocksInfo
            this.measurementInfo.rightLegInfo = this.getMeasurementInfo.rightLegInfo
            this.measurementInfo.leftLegInfo = this.getMeasurementInfo.leftLegInfo
            this.measurementInfo.rightCalfInfo = this.getMeasurementInfo.rightCalfInfo
            this.measurementInfo.leftCalfInfo = this.getMeasurementInfo.leftCalfInfo
            this.measurementInfo.relaxedRightArmInfo = this.getMeasurementInfo.relaxedRightArmInfo 
            this.measurementInfo.rightArmStressedInfo = this.getMeasurementInfo.rightArmStressedInfo
            this.measurementInfo.relaxedLeftArmInfo = this.getMeasurementInfo.relaxedLeftArmInfo
            this.measurementInfo.leftArmStressedInfo = this.getMeasurementInfo.leftArmStressedInfo
            return this.measurementInfo
        }
    },    
}