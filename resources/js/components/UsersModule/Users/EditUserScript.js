import datatables from 'datatables';
import download from 'downloadjs'
import { log } from 'util';
import { runInThisContext } from 'vm';
import {mapGetters,mapActions,mapMutations} from 'vuex'

export default {
    created() {
        
    },
    data() {
        return {
            user:{email: '', administrador: '', coach: '', atleta: '', plan_id: null , password: '', first_login: ''},
            roles: [
                { value: "administrador",text: "Administrador",disabled: false},
                { value: "coach", text: "Coach", disabled: false },
                { value: "atleta", text: "Atleta", disabled: false }
            ],  
            checkRoles:[],
            rolesRules: [v => v.length > 0 || "Debe asignar al menos un rol"],
            valid: false,
            alert:{
                visible: false,
                message:'',
                color:'',
                icon:''
            },
        }
    }, 
    methods:{
       ...mapActions('users',['getAllUsers','updateUser']),
       ...mapMutations('users',['setSnackbarU']),
       validate(){
            this.setSnackbarU(this.alert);
            this.user.administrador = false;
            this.user.coach = false;
            this.user.atleta = false;

            if (this.$refs.form.validate()) {
                for (let i = 0; i < this.checkRoles.length; i++) {
                    if (this.checkRoles[i] == "administrador") {
                        this.user.administrador = true;
                    } else if (this.checkRoles[i] == "coach") {
                        this.user.coach = true;
                    } else if (this.checkRoles[i] == "atleta") {
                        this.user.atleta = true;
                    }
                }
                this.updateUser(this.user);
            } else {
                this.alert.visible = true;
                this.alert.message = "Error en la creación";
                this.alert.color = "error";
                this.alert.icon = "mdi-close-octagon";
                this.setSnackbarU(this.alert);
            }
       },
       reset(){

       }
    },
    computed: {
        ...mapGetters('users',['getSnackbarU','getUserUpdated','getUserRolesUpdated']),
        snackbar(){
            this.alert.visible = this.getSnackbarU.visible
            this.alert.message = this.getSnackbarU.message
            this.alert.color = this.getSnackbarU.color
            this.alert.icon = this.getSnackbarU.icon
            return this.alert
        }
    },
    watch: {
        checkRoles:{
            handler(){
                if(this.checkRoles.length == 0){
                    this.roles[0].disabled = false;
                    this.roles[1].disabled = false;
                    this.roles[2].disabled = false;
                    this.visiblePlans = false;
                    this.user.plan_id = null
                } else {
                    for (let i = 0; i < this.checkRoles.length; i++) {
                        if (this.checkRoles[i] == "administrador" || this.checkRoles[i] == "coach") {
                            this.roles[2].disabled = true;
                        } else if (this.checkRoles[i] == "atleta") {
                            this.visiblePlans = true
                            this.roles[0].disabled = true;
                            this.roles[1].disabled = true;
                        } 

                    }
                }
                
            },
            deep:true
        },
        getUserUpdated:{
            handler(newValue){
                this.user.id = newValue.id
            },
            deep:true
        },
        getUserRolesUpdated:{
            handler(newValue){
                this.checkRoles = newValue
            },
            deep:true
        },

    },
    
}