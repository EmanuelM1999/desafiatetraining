import {mapActions,mapGetters,mapMutations} from 'vuex'

export default {
    created() {
        this.getAllPlans();
        this.setCurrentDate();
    },
    props:{
        userId:{
            type:Number
        }
    },
    data() {
        return {
            plans:[],
            payments:[],
            plan:'',
            payment:{id: '',typePlan:'', plan_id: '',paymentDate:'',expirationDate:'',discountPayment: '', planValue: 0,total: '', user_id: ''},
            userPayments:[],
            formRenewPayment: false,
            showUserPayTable: true,
            //Errores
            dateRules: [
                v => !!v || 'Este es un campo obligatorio',
            ],
            planRules: [
                v => !!v || 'Este es un campo obligatorio',
            ],
            discountRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido',
            ],
            valid:false,
            alert:{
                visible: false,
                message:'',
                color:'',
                icon:'',
                state:false
              },
        }
    }, 
    methods:{
        ////////////////
        ...mapActions('payments',['createPayment','getAllPlans','downloadpdf']),
        ...mapActions('users',['getAllUserPayments']),
        ...mapMutations('payments',['setSnackbarC']),
        
        showFormRenewPayment(){
            this.showUserPayTable = false;
            this.formRenewPayment = true;
        },
        HideFormRenewPayment(){
            this.formRenewPayment = false;
            this.showUserPayTable = true;
            this.plan = ''
            this.payment.expirationDate = ''
            this.payment.typePlan = ''
            this.payment.discountPayment = ''
        },
        emptyFormRenewPayment(){
            this.formRenewPayment = false;
            this.showUserPayTable = true;
            this.plan = ''
            this.payment.expirationDate = ''
            this.payment.typePlan = ''
            this.payment.discountPayment = ''
        },
        setCurrentDate(){
            var currentdate = new Date()
            var month = currentdate.getMonth() +1;
            var day = currentdate.getDate();
            var year = currentdate.getFullYear();
            if(day<10){
                day='0'+day
            }
            if(month<10){
                month='0'+month
            }

            this.payment.paymentDate = year + "-" + month + "-" + day 
        },

        validate(){
            if(this.$refs.form.validate()){
                this.payment.user_id = this.userId
                this.payment.typePlan = this.plan.name
                this.payment.plan_id = this.plan.id
                this.payment.planValue = this.plan.price
                if(this.payment.discountPayment == ''){
                    this.payment.discountPayment = 0 
                }
                this.payment.total = this.payment.planValue - this.payment.discountPayment
                this.createPayment(this.payment)
            }else{
                this.alert.visible = true;
                this.alert.message = "Error en la creación";
                this.alert.color = "error";
                this.alert.icon = "mdi-close-octagon";
                this.alert.state = false
                this.setSnackbarC(this.alert);
            }    
        },
    },
    watch: {
        getSnackbarC:{
            handler(){
                if(this.getSnackbarC.state == true){
                    this.HideFormRenewPayment()
                    this.getAllUserPayments(this.userId)
                }
            },
            deep:true
        }
    },
    computed: {
        ...mapGetters('users',['getUserPayments']),
        ...mapGetters('payments',['getPlans','getSnackbarC'])
        
    },
    
}