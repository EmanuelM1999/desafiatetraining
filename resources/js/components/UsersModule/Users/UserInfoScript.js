import {mapGetters,mapMutations,mapActions} from 'vuex'

export default {
    created() {
      
    },
    data() {
        return {
            user:{
                name:'',
                id_document:'',
                telephone:'',
                email:'',
                date_of_birth:'',
                name_contact_emergency:'',
                telephone_contact_emergency:'',
                photo:''
            },
        }
    }, 
    
    methods:{
        ...mapActions('users',['getUserInfo']),
        
    },
    computed: {
        ...mapGetters('users',['getUserInfoG']),
        userInfo(){
            this.user = this.getUserInfoG
            return this.user
        }
    },
    
}