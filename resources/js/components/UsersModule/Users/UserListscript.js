import datatables from 'datatables';
import download from 'downloadjs'
import { log } from 'util';
import { runInThisContext } from 'vm';
import {mapGetters,mapActions,mapMutations} from 'vuex'

export default {
    created() {
        this.getUserAuth();
        this.getUsers();
        this.userfiltro = this.users;
    },
    data() {
        return {

            user:{id: null, name: ' ', email: '', administrador: '', coach: '', atleta: '', plan_id: null , password: '', first_login: ''},  
            checkRoles:[], checkPlansC: '',
            
            //Usuarios
            users:[],
            userfiltro:[],
            user_auth:{},
            userId:0,
            roles:[
                {value: "administrador", text: "Administrador"},
                {value: "coach", text: "Coach"},
                {value: "atleta", text: "Atleta"}
            ],
            userToSearch:'',
            searchedUsers:[],
        }
    }, 
    methods:{
        ...mapActions('users',['getAllUsers','getAllPlans','getUserInfo','getUserObservations','getUserMeasurements','getAllUserPayments']),
        ...mapMutations('users',['setUserUpdated','setUserRolesUpdated']),
        setUserId(id){
            this.userId = id
            this.getAllUserPayments(id)
            
        },
        setUser(item){
            this.checkRoles = []
            this.user.id = item.id
            console.log(this.user.id);

            if(item.administrador === 1){
                this.checkRoles.push("administrador")
            }
            if(item.coach === 1){
                this.checkRoles.push("coach")
            }
            if(item.atleta === 1){
                this.checkRoles.push("atleta")
            }

            this.setUserUpdated(this.user)
            this.setUserRolesUpdated(this.checkRoles)
        },
        ////////////////
        getUserAuth(){
            axios.get('/get-user-auth')
                .then(response => {
                  console.log(response.data);                     
                    this.user_auth = response.data;                                   
                })
                .catch(function (error) {
                     console.log(error);
              });
              
        },
        
        changeNumberRol(){
            for (let i = 0; i < this.users.length; i++) {
                if(this.users[i].administrador == 1){
                    this.users[i].administrador == 'administrador'
                }
                
            }
        },
        getUsers(){
            axios.get('/user')
                .then(response => {                     
                    this.users = response.data;
                    this.userfiltro = response.data;
                    for (let i = 0; i < this.userfiltro.length; i++) {
                        this.userfiltro[i].name = this.userfiltro[i].name +' '+ this.userfiltro[i].surnames;
                        
                    }
                    console.log(this.users);                                    
                })
                .catch(function (error) {
                     console.log(error);
                });
                
        },
        
    },
    computed: {
        ...mapGetters('users',['getPlans']),
        filtro:{
            get(){
            return this.userToSearch
            },
            set(value){
            console.log('filtro ejecutado!');
            this.userToSearch = value.toLowerCase()

            // value = value.toLowerCase();
            this.userfiltro = this.users.filter((item) => item.name.toLowerCase().includes(this.userToSearch) || item.email.toLowerCase().includes(this.userToSearch)
            || item.telephone.includes(this.userToSearch))
            // this.userfiltro = this.users.filter((item) => item.email.toLowerCase().includes(this.userToSearch))
            
            }
        },
    },
    
}