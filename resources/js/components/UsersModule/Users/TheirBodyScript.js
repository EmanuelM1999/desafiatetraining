import {mapGetters,mapActions,mapMutations} from 'vuex'

export default {
    data() {
        return {
            
            measurement_1_Rules: [v => !!v || "Debe seleccionar un registro"],
            measurement_2_Rules: [v => !!v || "Debe seleccionar un registro"],
            valid: false,
            nameUserInfo: "",
            //Medidas de los usuarios
            userMeasurements: [],
            measurement: {
                date: "",
                weight: "",
                height: "",
                imc: "",
                fat: "",
                muscle: "",
                caloricExpenditure: "",
                metabolicAge: "",
                visceralFat: "",
                chest: "",
                highAbdomen: "",
                lowerAbdomen: "",
                waist: "",
                buttocks: "",
                rightLeg: "",
                leftLeg: "",
                rightCalf: "",
                leftCalf: "",
                relaxedRightArm: "",
                rightArmStressed: "",
                relaxedLeftArm: "",
                leftArmStressed: ""
            },

            measurement2: {
                date: "",
                weight: "",
                height: "",
                imc: "",
                fat: "",
                muscle: "",
                caloricExpenditure: "",
                metabolicAge: "",
                visceralFat: "",
                chest: "",
                highAbdomen: "",
                lowerAbdomen: "",
                waist: "",
                buttocks: "",
                rightLeg: "",
                leftLeg: "",
                rightCalf: "",
                leftCalf: "",
                relaxedRightArm: "",
                rightArmStressed: "",
                relaxedLeftArm: "",
                leftArmStressed: ""
            },

            measurementResult: {
                date: "Total",
                weight: "",
                height: "",
                imc: "",
                fat: "",
                muscle: "",
                caloricExpenditure: "",
                metabolicAge: "",
                visceralFat: "",
                chest: "",
                highAbdomen: "",
                lowerAbdomen: "",
                waist: "",
                buttocks: "",
                rightLeg: "",
                leftLeg: "",
                rightCalf: "",
                leftCalf: "",
                relaxedRightArm: "",
                rightArmStressed: "",
                relaxedLeftArm: "",
                leftArmStressed: ""
            },

            measurements: [],
            measurements2: [],
            measurementsResult: [],
            namesMeasurements:['Fecha de registro','Peso','Estatura','IMC','Grasa','% de músculo','Gasto calórico','Edad metabólica','Grasa visceral',
            'Pecho','Abdomen alto','Abdomen bajo','Cintura','Glúteos','Pierna derecha','Pierna izquierda','Pantorrilla derecha','Pantorrilla izquierda',
            'Brazo derecho relajado','Brazo derecho tensionado','Brazo izquierdo relajado','Brazo izquierdo tensionado',],
            firstDateSelected: {},
            secondDateSelected: {},
        };
    },
    methods: {
        ///// Metodos para obtener los datos de los 2 registros a comparar
        getMeasurement_1() {
            this.measurement.date = this.firstDateSelected.date;
            this.measurement.weight = this.firstDateSelected.weight;
            this.measurement.height = this.firstDateSelected.height;
            this.measurement.imc = this.firstDateSelected.imc;
            this.measurement.fat = this.firstDateSelected.fat;
            this.measurement.muscle = this.firstDateSelected.muscle;
            this.measurement.caloricExpenditure = this.firstDateSelected.caloricExpenditure;
            this.measurement.metabolicAge = this.firstDateSelected.metabolicAge;
            this.measurement.visceralFat = this.firstDateSelected.visceralFat;
            this.measurement.chest = this.firstDateSelected.chest;
            this.measurement.highAbdomen = this.firstDateSelected.highAbdomen;
            this.measurement.lowerAbdomen = this.firstDateSelected.lowerAbdomen;
            this.measurement.waist = this.firstDateSelected.waist;
            this.measurement.buttocks = this.firstDateSelected.buttocks;
            this.measurement.rightLeg = this.firstDateSelected.rightLeg;
            this.measurement.leftLeg = this.firstDateSelected.leftLeg;
            this.measurement.rightCalf = this.firstDateSelected.rightCalf;
            this.measurement.leftCalf = this.firstDateSelected.leftCalf;
            this.measurement.relaxedRightArm = this.firstDateSelected.relaxedRightArm;
            this.measurement.rightArmStressed = this.firstDateSelected.rightArmStressed;
            this.measurement.relaxedLeftArm = this.firstDateSelected.relaxedLeftArm;
            this.measurement.leftArmStressed = this.firstDateSelected.leftArmStressed;
                
            console.log(this.measurement.date);
        },
        getMeasurement_2() {
                    
            this.measurement2.date = this.secondDateSelected.date;
            this.measurement2.weight = this.secondDateSelected.weight;
            this.measurement2.height = this.secondDateSelected.height;
            this.measurement2.imc = this.secondDateSelected.imc;
            this.measurement2.fat = this.secondDateSelected.fat;
            this.measurement2.muscle = this.secondDateSelected.muscle;
            this.measurement2.caloricExpenditure = this.secondDateSelected.caloricExpenditure;
            this.measurement2.metabolicAge = this.secondDateSelected.metabolicAge;
            this.measurement2.visceralFat = this.secondDateSelected.visceralFat;
            this.measurement2.chest = this.secondDateSelected.chest;
            this.measurement2.highAbdomen = this.secondDateSelected.highAbdomen;
            this.measurement2.lowerAbdomen = this.secondDateSelected.lowerAbdomen;
            this.measurement2.waist = this.secondDateSelected.waist;
            this.measurement2.buttocks = this.secondDateSelected.buttocks;
            this.measurement2.rightLeg = this.secondDateSelected.rightLeg;
            this.measurement2.leftLeg = this.secondDateSelected.leftLeg;
            this.measurement2.rightCalf = this.secondDateSelected.rightCalf;
            this.measurement2.leftCalf = this.secondDateSelected.leftCalf;
            this.measurement2.relaxedRightArm = this.secondDateSelected.relaxedRightArm;
            this.measurement2.rightArmStressed = this.secondDateSelected.rightArmStressed;
            this.measurement2.relaxedLeftArm = this.secondDateSelected.relaxedLeftArm;
            this.measurement2.leftArmStressed = this.secondDateSelected.leftArmStressed;
            console.log(this.measurement2.date);
        },
        toCompareMeasurements() {
            this.measurements = []
            this.measurementResult.weight = "";
            this.measurementResult.height = "";
            this.measurementResult.imc = "";
            this.measurementResult.fat = "";
            this.measurementResult.muscle = "";
            this.measurementResult.caloricExpenditure = "";
            this.measurementResult.metabolicAge = "";
            this.measurementResult.visceralFat = "";
            this.measurementResult.chest = "";
            this.measurementResult.highAbdomen = "";
            this.measurementResult.lowerAbdomen = "";
            this.measurementResult.waist = "";
            this.measurementResult.buttocks = "";
            this.measurementResult.rightLeg = "";
            this.measurementResult.leftLeg = "";
            this.measurementResult.rightCalf = "";
            this.measurementResult.leftCalf = "";
            this.measurementResult.relaxedRightArm = "";
            this.measurementResult.rightArmStressed = "";
            this.measurementResult.relaxedLeftArm = "";
            this.measurementResult.leftArmStressed = "";
            
            if (this.$refs.form.validate()) {
                console.log(this.measurements);
                if (this.measurement2.date > this.measurement.date) {
                    this.measurementResult.weight = this.measurement2.weight - this.measurement.weight;
                    this.measurementResult.height = this.measurement2.height - this.measurement.height;
                    this.measurementResult.imc = this.measurement2.imc - this.measurement.imc;
                    this.measurementResult.fat = this.measurement2.fat - this.measurement.fat;
                    this.measurementResult.muscle = this.measurement2.muscle - this.measurement.muscle;
                    this.measurementResult.caloricExpenditure = this.measurement2.caloricExpenditure - this.measurement.caloricExpenditure;
                    this.measurementResult.metabolicAge = this.measurement2.metabolicAge - this.measurement.metabolicAge;
                    this.measurementResult.visceralFat = this.measurement2.visceralFat - this.measurement.visceralFat;
                    this.measurementResult.chest = this.measurement2.chest - this.measurement.chest;
                    this.measurementResult.highAbdomen = this.measurement2.highAbdomen - this.measurement.highAbdomen;
                    this.measurementResult.lowerAbdomen = this.measurement2.lowerAbdomen - this.measurement.lowerAbdomen;
                    this.measurementResult.waist = this.measurement2.waist - this.measurement.waist;
                    this.measurementResult.buttocks = this.measurement2.buttocks - this.measurement.buttocks;
                    this.measurementResult.rightLeg = this.measurement2.rightLeg - this.measurement.rightLeg;
                    this.measurementResult.leftLeg = this.measurement2.leftLeg - this.measurement.leftLeg;
                    this.measurementResult.rightCalf = this.measurement2.rightCalf - this.measurement.rightCalf;
                    this.measurementResult.leftCalf = this.measurement2.leftCalf - this.measurement.leftCalf;
                    this.measurementResult.relaxedRightArm = this.measurement2.relaxedRightArm - this.measurement.relaxedRightArm;
                    this.measurementResult.rightArmStressed = this.measurement2.rightArmStressed - this.measurement.rightArmStressed;
                    this.measurementResult.relaxedLeftArm = this.measurement2.relaxedLeftArm - this.measurement.relaxedLeftArm;
                    this.measurementResult.leftArmStressed = this.measurement2.leftArmStressed - this.measurement.leftArmStressed;
                    this.measurements.push(this.measurement)
                    this.measurements.push(this.measurement2)
                    console.log('tamaño measurements' + this.measurements.length);
                    console.log(this.measurements);
                } else {
                    this.measurementResult.weight = this.measurement.weight - this.measurement2.weight;
                    this.measurementResult.height = this.measurement.height - this.measurement2.height;
                    this.measurementResult.imc = this.measurement.imc - this.measurement2.imc;
                    this.measurementResult.fat = this.measurement.fat - this.measurement2.fat;
                    this.measurementResult.muscle = this.measurement.muscle - this.measurement2.muscle;
                    this.measurementResult.caloricExpenditure = this.measurement.caloricExpenditure - this.measurement2.caloricExpenditure;
                    this.measurementResult.metabolicAge = this.measurement.metabolicAge - this.measurement2.metabolicAge;
                    this.measurementResult.visceralFat = this.measurement.visceralFat - this.measurement2.visceralFat;
                    this.measurementResult.chest = this.measurement.chest - this.measurement2.chest;
                    this.measurementResult.highAbdomen = this.measurement.highAbdomen - this.measurement2.highAbdomen;
                    this.measurementResult.lowerAbdomen = this.measurement.lowerAbdomen - this.measurement2.lowerAbdomen;
                    this.measurementResult.waist = this.measurement.waist - this.measurement2.waist;
                    this.measurementResult.buttocks = this.measurement.buttocks - this.measurement2.buttocks;
                    this.measurementResult.rightLeg = this.measurement.rightLeg - this.measurement2.rightLeg;
                    this.measurementResult.leftLeg = this.measurement.leftLeg - this.measurement2.leftLeg;
                    this.measurementResult.rightCalf = this.measurement.rightCalf - this.measurement2.rightCalf;
                    this.measurementResult.leftCalf = this.measurement.leftCalf - this.measurement2.leftCalf;
                    this.measurementResult.relaxedRightArm = this.measurement.relaxedRightArm - this.measurement2.relaxedRightArm;
                    this.measurementResult.rightArmStressed = this.measurement.rightArmStressed - this.measurement2.rightArmStressed;
                    this.measurementResult.relaxedLeftArm = this.measurement.relaxedLeftArm - this.measurement2.relaxedLeftArm;
                    this.measurementResult.leftArmStressed = this.measurement.leftArmStressed - this.measurement2.leftArmStressed;
                    this.measurements.push(this.measurement)
                    this.measurements.push(this.measurement2)
                    this.measurementsResult.push(this.measurementResult)
                    console.log('tamaño measurements' + this.measurements.length);
                    console.log('tamaño measurements: ' + this.measurements.length);
                    console.log(this.measurements);
                }
            } else {
                console.log('error');
            }
        },
        reset(){
            this.firstDateSelected = ''
            this.secondDateSelected = ''
            this.measurements = []
        }
    },
    computed: {
        ...mapGetters('users',['getMeasurements']),
    },
};