import { mapActions, mapGetters, mapMutations } from "vuex";

export default {
    created() {
        this.getAllPlans();
    },
    data() {
        return {
            user: {
                email: "",
                administrador: false,
                coach: false,
                atleta: false,
                plan_id: null,
                password: "",
                first_login: ""
            },
            checkRoles: [],
            roles: [
                { value: "administrador",text: "Administrador",disabled: false},
                { value: "coach", text: "Coach", disabled: false },
                { value: "atleta", text: "Atleta", disabled: false }
            ],
            plans: [],
            dialog: false,
            valid: false,
            visiblePlans: false,
            emailRules: [
                v => !!v || "Este es un campo obligatorio",
                v => /.+@.+\..+/.test(v) || 'Ingrese un correo válido. Ej: dst@gmail.com'
            ],
            rolesRules: [v => v.length > 0 || "Debe asignar al menos un rol"],
            planRules: [v => v != null || "Debe seleccionar un plan"],
            alert:{
                visible: false,
                message:'',
                color:'',
                icon:''
            },
        };
    },

    methods: {
        ...mapActions('users', ['getAllPlans','createUser']),
        ...mapMutations('users', ['setSnackbar']),

        validate() {
            this.setSnackbar(this.alert);
            this.user.administrador = false;
            this.user.coach = false;
            this.user.atleta = false;
            this.user.first_login = false;

            if (this.$refs.form.validate()) {
                for (let i = 0; i < this.checkRoles.length; i++) {
                    if (this.checkRoles[i] == "administrador") {
                        this.user.administrador = true;
                    } else if (this.checkRoles[i] == "coach") {
                        this.user.coach = true;
                    } else if (this.checkRoles[i] == "atleta") {
                        this.user.atleta = true;
                    }
                }
                this.createUser(this.user);
            } else {
                this.alert.visible = true;
                this.alert.message = "Error en la creación";
                this.alert.color = "error";
                this.alert.icon = "mdi-close-octagon";
                this.setSnackbar(this.alert);
            }
        },
        reset() {
            this.$refs.form.reset()
            this.dialog = false;
        },
    },
    computed: {
        ...mapGetters('users', ['getPlans','getSnackbarC']),
        getPlansC() {
            this.plans = this.getPlans;
            return this.plans;
        },
        snackbar(){
            this.alert.visible = this.getSnackbarC.visible
            this.alert.message = this.getSnackbarC.message
            this.alert.color = this.getSnackbarC.color
            this.alert.icon = this.getSnackbarC.icon
            return this.alert
        }
    },
    watch: {
        checkRoles:{
            handler(){
                if(this.checkRoles.length == 0){
                    this.roles[0].disabled = false;
                    this.roles[1].disabled = false;
                    this.roles[2].disabled = false;
                    this.visiblePlans = false;
                    this.user.plan_id = null
                } else {
                    for (let i = 0; i < this.checkRoles.length; i++) {
                        if (this.checkRoles[i] == "administrador" || this.checkRoles[i] == "coach") {
                            this.roles[2].disabled = true;
                        } else if (this.checkRoles[i] == "atleta") {
                            this.visiblePlans = true
                            this.roles[0].disabled = true;
                            this.roles[1].disabled = true;
                        } 

                    }
                }
                
            },
            deep:true
        }
    },
};

//  getPlans(){

//     //  if($('#userRole0').prop('checked') || $('#userRole1').prop('checked')){
//     //       $('#userRole2').prop('disabled', true);
//     //  }else{
//     //       $('#userRole0').prop('disabled', false);
//     //       $('#userRole1').prop('disabled', false);
//     //       $('#userRole2').prop('disabled', false);
//     //  }

//     //  if($('#userRole2').prop('checked')){
//     //        axios.get('/plans')
//     //        .then(response => {
//     //        this.plans = response.data;
//     //        })
//     //        .catch(function (error) {
//     //        console.log(error);
//     //        });
//     //       $('#userRole0').prop('disabled', true);
//     //       $('#userRole1').prop('disabled', true);
//     //  }else{
//     //       this.errorCheckPlan = false
//     //       this.plans = [];
//     //       this.checkPlans = ''
//     //       $('#userRole0').prop('disabled', false);
//     //       $('#userRole1').prop('disabled', false);

//     //  }

// },
