import {mapGetters,mapActions,mapMutations} from 'vuex'

export default {
    data() {
        return {
            observations:[]
        };
    },
    methods: {
       

    },
    computed:{
        ...mapGetters('users',['getObservations'])
    }
};