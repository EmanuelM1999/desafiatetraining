import {mapActions,mapGetters} from 'vuex'

export default {
    mounted() {
        this.showReports()
    },
    data() {
        return {
            plans:[],
            users:[],
            usersbyplan:[],
            agerange:['12-17', '18-24', '25-34', '35-44', '45+'],
            ages:[],
            usersbyage:[],
            names:[],
            options:null,
            loaded:false,
            options: {
                scales: {
                    yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        stepSize: 1,
                    },
                    gridLines: {
                        display: true  
                    }
                    }],
                    xAxes: [{
                    gridLines: {
                        display: true
                    }
                    }]
                },
                legend: {
                    display: true
                },
                responsive: true,
                maintainAspectRatio: false,
            }

        }
    },
    methods: {
        ...mapActions('reports',['getUsersAthletes','getAllPlans','getUsersAges']),
        showReports(){
            this.loaded = true
            this.showReportAges()
            this.showReportStudentsByPlan()
        },
        showReportStudentsByPlan(){
            this.getUsersAthletes();
            this.getAllPlans();
            
        },
        showReportAges(){ 
            this.getUsersAges();
        },
    },
    computed: {
        chartdata1(){
                return {
                    labels: this.agerange,
                    datasets:[
                        {   
                            label:'# atletas',
                            backgroundColor: '#006ca2',
                            borderColor: ["rgba(255,99,132,0.2)","rgba(54,162,235,0.2)"],
                            borderWidth: 1,
                            data:this.getAgesC,
                        },
                    ]
                }
                
        },
        chartdata2(){
            return {
                labels: this.getPlansNameC,
                datasets:[
                    {   
                        label:'# atletas',
                        backgroundColor: '#ffb900',
                        borderColor: ["rgba(255,99,132,0.2)","rgba(54,162,235,0.2)"],
                        borderWidth: 1,
                        data:this.getPlansC,
                    },
                ]
            }
                
        },
        ...mapGetters('reports',['getAthletes','getPlans','getAges','getPlansName']),
        getAthletesC(){
            this.users = this.getAthletes
            return this.users
        },
        getPlansC(){
            this.plans = this.getPlans
            return this.plans
        },
        getAgesC(){
            this.ages = this.getAges
            return this.ages
        },
        getPlansNameC(){
            this.names = this.getPlansName
            return this.names
        }
    },
}