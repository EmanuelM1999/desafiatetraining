import {mapGetters,mapActions, mapMutations} from 'vuex'

export default {
    data() {
        return {
           
            typesId: [
                {value: 'CC', text: 'Cédula de ciudadanía'},
                {value: 'TI', text: 'Tarjeta de identidad'},
                {value: 'CE', text: 'Tarjeta de extranjería'}
            ],
            typeIdRules: [
                v => !!v || 'Este es un campo obligatorio',
            ],
            idRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido. Solo se permiten números',
            ],
            nameRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[ñA-Za-z _]*[ñA-Za-z][ñA-Za-z _]*$/ .test(v) || 'Solo se permiten letras',
            ],
            emailRules: [
                v => !!v || "Este es un campo obligatorio",
                v => /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(v) || 'Ingrese un correo válido. Ej: dst@gmail.com'
            ],
            telephoneRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido. Solo se permiten números',
            ],
            valid:false,
            dialog: false,
            loger: [],
            request: {typeId: '', id: '', name: '', email: '', telephone: '', message: ''},
            alert:{
                visible: false,
                message:'',
                color:'',
                icon:''
            },
            
        }
    },

    methods:{
        ...mapActions('freeClass',['createRequest']),
        ...mapMutations('freeClass',['setSnackbarC']),
        validate(){
            if(this.$refs.form.validate()){
                this.createRequest(this.request)
            }else{
                this.alert.visible = true
                this.alert.message = "Verifique que los campos obligatorios estén correctos"
                this.alert.color = "error"
                this.alert.icon = "mdi-close-octagon";
                this.setSnackbarC(this.alert)
            }
        },
        reset () {
            this.$refs.form.reset()
            this.dialog = false
        },
        validEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
    },

    computed: {
        ...mapGetters('freeClass',['getSnackbarC'])
    },
}