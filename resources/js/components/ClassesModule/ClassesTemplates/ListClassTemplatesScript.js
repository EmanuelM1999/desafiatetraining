export default {
    created() {
        this.getTemplates();        
        axios.get('/query-coaches')
                .then(response => {                     
                    this.coaches = response.data;                    
                })
                .catch(function (error) {
                     console.log(error);
                });

    },
    data() {
        return {
            horas:['1','2','3','1','2','3','1','2','3'],
            //Trabaja con este objeto para crear plantillas
            template:{name:"", idTemplate:''},
            //Arreglo que guardd las plantillas creadas
            templates:[],
            //Objeto para crear una clase
            lessonC:{idC:'',typeC:'',hourC:'',dayC:'',coachC:'',placesC:''},
            lesson:{id:'',type:'',hour:'',day:'',coach:'',places:''},
            classes:[],
            //Clases para el select
            typeClasses:[
                {text: 'Wod', value:'wod'},
                {text: 'Wod online', value:'wod_online'}
            ],
            //Dias para el v-select
            days:[
                {text: 'Lunes', value: 'lunes'},
                {text: 'Martes', value: 'martes'},
                {text: 'Miércoles', value: 'miercoles'},
                {text: 'Jueves', value: 'jueves'},
                {text: 'Viernes', value: 'viernes'},
                {text: 'Sábado', value: 'sabado'},
                {text: 'Domingo', value: 'domingo'}
                ],
            //Se guardan los dias seleccionados en la creacion de clases
            daysSelectedC:[],
            daysSelected:'',
            h5:[{lunes:[{text:'lunes5'},{text:'lunes25'}]},
                {martes:[{text:'martes5'},{text:'martes25'}]}],
            h6:[{lunes:[{text:'lunes6'},{text:'lunes26'}]},
                {martes:[]}],    
            //Para guardar una clase dependiendo de su dia y mostrarla en la tabla
            monday:[],
            tuesday:[], 
            wednesday:[], 
            thursday:[],
            friday:[], 
            saturday:[], 
            sunday:[],
            
                
            // Para cargar los profesores en el v-select
            coaches:[],
            coachesSelectedC:'',
            coachesSelected:'',

            //Variables para manejar los menús
            visibleEditTemplate:false, visibleAddTemplate:true, visibleBtnAddClass:true, visibleAddClass:false,
            //Errores
            errorEmptySpaces:false, errorClassType: false, errorHour: false, errorDays: false, errorCoaches: false,
            errorPlaces: false, errorDaysC: false, errorCoachesC: false,
            //Intervalo
            interval1:'',interval2:'',interval3:'',interval4:''
        }
    },
    methods: {
        
        showEditTemplate(item){
            this.template.name = item.name_template 
            this.template.idTemplate = item.id
            this.visibleAddTemplate = false
            this.visibleEditTemplate = true
            this.errorEmptySpaces = false
            this.getClasses();
            
        },
        showAddTemplate(){
            this.template.name = '' 
            this.template.idTemplate = ''
            this.visibleEditTemplate = false
            this.visibleAddTemplate = true
        },
        showAddClass(){
            this.visibleBtnAddClass = false
            this.visibleAddClass = true
        },
        hideAddClass(){
            this.visibleAddClass = false
            this.visibleBtnAddClass = true
        },
        emptyForm(){
            this.lessonC.typeC = ''  
            this.lessonC.hourC = ''
            this.daysSelectedC = []
            this.coachesSelectedC = ''
            this.lessonC.placesC = ''
            this.errorClassType = false
            this.errorHour = false
            this.errorDays = false
            this.errorCoaches = false
            this.errorPlaces = false
        },

        addTemplate(item){
            if(this.template.name !=''){
             axios.post('/class-template',{name_template: this.template.name})
                .then(response => {                     
                    if(response.status == 201){
                        this.flashMessage.success({
                            status: 'success',
                            message: 'Creación exitosa',
                            time: 2000,
                        });
                        this.templates = []
                        this.getTemplates();
                        this.errorEmptySpaces = false
                        this.template.name = ''
                    }                    
                })
                .catch(function (error) {
                     console.log(error);                     
                });
            }else{
                this.errorEmptySpaces = true
            }
        },
        getTemplates(){
            axios.get('/class-template')
                .then(response => {                     
                    this.templates = response.data;                    
                })
                .catch(function (error) {
                     console.log(error);
                });
        },
        
        // Eliminar plantilla
        
        idDeleteTemplate(item){
            this.template.idTemplate = item.id            
        },

        deleteTemplate(){
            
             axios.delete(`/class-template/${this.template.idTemplate}`)
                .then(response => {                     
                    this.flashMessage.success({
                        status: 'success',
                        message: 'Eliminada con éxito',
                        time: 1500,
                    });  
                    this.templates = []
                    this.getTemplates();
                    $('#deleteTemplateModal .close').click()                                        
                })
                .catch(function (error) {
                     console.log(error);
                });           

        },
        //Fin Eliminar plantilla
        
        //Clases

        getClasses(){
              axios.post('/query-classes',{
                id: this.template.idTemplate,                   
            })
                .then(response => {                     
                    
                    if(response.status == 200){     
                        this.monday = [];
                        this.tuesday = [];
                        this.thursday = [];
                        this.wednesday = [];
                        this.sunday = [];
                        this.saturday = [];
                        this.friday = [];   
                                        
                        for (let i = 0; i < response.data.length; i++) {
                            if (response.data[i].day == 'lunes') {
                                this.monday.push(response.data[i]);
                            } else if (response.data[i].day == 'martes') {
                                console.log("adssad "+ response.data[i]);
                                this.tuesday.push(response.data[i]);
                            }else if (response.data[i].day == 'miercoles') {
                                this.wednesday.push(response.data[i]);
                            } else if (response.data[i].day == 'jueves') {
                                this.thursday.push(response.data[i]);
                            } else if (response.data[i].day == 'viernes') {
                                this.friday.push(response.data[i]);
                            }else if (response.data[i].day == 'sabado') {
                                this.saturday.push(response.data[i]);
                            }else if (response.data[i].day == 'domingo') {
                                this.sunday.push(response.data[i]);
                            }
                                                        
                        }                                             
                    }                                        
                })
                .catch(function (error) {
                     console.log(error);                     
            });   
        },
        
        addClass(){
        
        if (this.lessonC.typeC == '') {
            this.errorClassType = true
        }
        else{this.errorClassType = false}
        
        if (this.lessonC.hourC == '') {
            this.errorHour = true
        }
        else{this.errorHour = false}

        if (this.daysSelectedC.length == 0) {
            this.errorDays = true
        }
        else{this.errorDays = false}
        
        if (this.coachesSelectedC == '') {
            this.errorCoaches = true
        }
        else{this.errorCoaches = false}

        if (this.lessonC.placesC == '' || this.lessonC.placesC == 0) {
            this.errorPlaces = true
        }
        else{this.errorPlaces = false}

        if (this.lessonC.typeC != '' && this.lessonC.hourC != '' && this.daysSelectedC.length != 0 && this.coachesSelectedC != ''
                && this.lessonC.placesC != '' && this.lessonC.placesC != 0) {
            let wodValue;
            let wod_onlineValue;
            if (this.lessonC.typeC == 'wod') {
                wodValue = true;
                wod_onlineValue = false;
            } else {
                wodValue = false;
                wod_onlineValue = true;
            }
            for (let index = 0; index < this.daysSelectedC.length; index++) {
                
                axios.post('/class-schedule',{
                    id_template: this.template.idTemplate,
                    wod: wodValue,
                    wod_online: wod_onlineValue,
                    places: this.lessonC.placesC,
                    day: this.daysSelectedC[index],
                    hour: this.lessonC.hourC,
                    coach: this.coachesSelectedC,     
                })
                    .then(response => {
                        console.log(response.status);                     
                        if(response.status == 200){
                            this.flashMessage.success({
                                status: 'success',
                                message: 'Creación exitosa',
                                time: 2000,
                            });
                            this.lessonC.typeC = ''  
                            this.lessonC.hourC = ''
                            this.daysSelectedC = []
                            this.coachesSelectedC = ''
                            this.lessonC.placesC = ''
                            this.errorClassType = false
                            this.errorHour = false
                            this.errorDays = false
                            this.errorCoaches = false
                            this.errorPlaces = false
                            this.getClasses();                      
                        }                    
                    })
                    .catch(function (error) {
                        console.log(error);                     
                });   
            }
        }
        },

        idClass(item){
            this.lesson.id = item.id
            if(item.wod == 1){
                this.lesson.type = 'wod'
            }else{
                this.lesson.type = 'wod_online'
            }

            this.lesson.hour = item.hour


            if (item.day === 'lunes') {
                this.daysSelected = 'lunes'
            }
            else if (item.day === 'martes') {
                this.daysSelected = 'martes'
            }
            else if (item.day === 'miercoles') {
                this.daysSelected = 'miercoles'
            }
            else if (item.day === 'jueves') {
                this.daysSelected = 'jueves'
            }
            else if (item.day === 'viernes') {
                this.daysSelected = 'viernes'
            }
            else if (item.day === 'sabado') {
                this.daysSelected = 'sabado'
            }
            else if (item.day === 'domingo') {
                this.daysSelected = 'domingo'
            }
                        
            this.coachesSelected = item.coach_id
            this.lesson.places = item.places                        
        },
        
        //Editar clase
        editClass(){
        if (this.lesson.type == '') {
            this.errorClassType = true
        }
        else{this.errorClassType = false}
        
        if (this.lesson.hour == '') {
            this.errorHour = true
        }
        else{this.errorHour = false}

        if (this.daysSelected == null || this.daysSelected == '') {
            this.errorDays = true
        }
        else{this.errorDays = false}
        
        if (this.coachesSelected == null || this.coachesSelected == '') {
            this.errorCoaches = true
        }
        else{this.errorCoaches = false}

        if (this.lesson.places == '' || this.lesson.places == 0) {
            this.errorPlaces = true
        }
        else{this.errorPlaces = false}

        if (this.lesson.type != '' && this.lesson.hour != '' && this.daysSelected != null && this.daysSelected != '' && this.coachesSelected != null
                && this.coachesSelected != '' && this.lesson.places != '' && this.lesson.places != 0) {

            let wodValue;
            let wod_onlineValue;
            if (this.lesson.type == 'wod') {
                wodValue = true;
                wod_onlineValue = false;
            } else {
                wodValue = false;
                wod_onlineValue = true;
            }
                axios.put(`/class-schedule/${this.lesson.id}`,{                
                    wod: wodValue,
                    wod_online: wod_onlineValue,
                    places: this.lesson.places,
                    day: this.daysSelected,
                    hour: this.lesson.hour,
                    coach: this.coachesSelected,
                })
                    .then(response => {
                        console.log(response.status);
                        if (response.status == 200) {
                            this.flashMessage.success({
                                status: 'success',
                                message: 'Actualiazción exitosa',
                                time: 2000,
                            });
                            this.classes= [];                                      
                            this.getClasses();
                            this.lesson.type = ''  
                            this.lesson.hour = ''
                            this.daysSelected = ''
                            this.coachesSelected = ''
                            this.lesson.places = ''
                            this.errorClassType = false
                            this.errorHour = false
                            this.errorDays = false
                            this.errorCoaches = false
                            this.errorPlaces = false       
                        }
                        
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        }, 
        deleteClass(){
             axios.delete(`/class-schedule/${this.lesson.id}`)
                .then(response => {
                    this.flashMessage.success({
                        status: 'success',
                        message: 'Eliminada con éxito',
                        time: 1500,
                    });
                    this.monday = [];
                        this.tuesday = [];
                        this.thursday = [];
                        this.wednesday = [];
                        this.sunday = [];
                        this.saturday = [];
                        this.friday = [];
                    this.getClasses();
                    $("#deleteClassM .close").click()                                                                             
                })
                .catch(function (error) {
                     console.log(error);
                });            
        }
    },
}
