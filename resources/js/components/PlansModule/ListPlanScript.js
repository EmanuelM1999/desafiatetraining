import datatables from 'datatables'
import { mapState, mapActions, mapGetters, mapMutations } from 'vuex'

export default {     
    data() {
        return {
            a:false,
            perPage: 5,

            plans:[],
            plan:{id: null, name: '',price: '',wod:'',wod_online:'' },
            checkClasses:[],
            search: '',
            headers: [
            { text: 'Nombre', align: 'start', value: 'name'},
            { text: 'Valor', value: 'price' },
            { text: 'Wod', value: 'wod' },
            { text: 'Wod online', value: 'wod_online' },
            { text: 'Acciones', value: 'actions', sortable: false },
            ],
        }
    }, 
    computed: {
        ...mapGetters('plans',
            ['getAll','getDialog']
        ),
        list(){
            this.plans = this.getAll;
            return this.plans
        },
    },    
    methods:{
        setInfoPlan(item){
            this.checkClasses = []
            this.plan.id = item.id
            this.plan.name = item.name
            this.plan.price = item.price
            this.plan.wod = item.wod
            this.plan.wod_online = item.wod_online

            if(this.plan.wod === 1){ 
                this.checkClasses.push('wod');
            }
            if(this.plan.wod_online === 1){               
                this.checkClasses.push('wod online');
            }
            this.setPlan(this.plan);
            this.setClasses(this.checkClasses);
        },

        ...mapActions('plans',
            ['getAllPlans','showPlan','dialogAction']
        ),
        ...mapMutations('plans', ['setPlan', 'setClasses', 'setPlanD']),
        
        listPlans(){
            this.plans = []
            this.getAllPlans();
        },        
    },
    created() {        
        this.listPlans();
    },
    
}
