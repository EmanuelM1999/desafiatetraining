import { mapActions, mapGetters, mapMutations } from 'vuex'

export default {
    created() {

    },
    data() {
        return {
            alert:{
                visible: false,
                message:'',
                color:'',
                icon:''
            },
            valid: false,
            nameRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[ñA-Za-z _]*[ñA-Za-z][ñA-Za-z _]*$/ .test(v) || 'Solo se permiten letras',
            ],
            priceRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido',
            ],
            classesRules: [
                v =>  v.length>0 || 'Selecciona al menos un tipo de clase'
            ],
            //Crear planes
            plansC:[],
            planC:{name: '',price: '',wod:'',wod_online:'' },
            checkClassesC:[],
            classes:[ 
                {value: 'wod', text: 'Wod'},
                {value: 'wod online', text: 'Wod online'}
            ],
            
        }
    },
    methods:{
        ...mapActions('plans',['getAllPlans','createPlan']),
        ...mapMutations('plans', ['setSnackbar']),
        
      validate2() {
            this.alert.visible = false
            this.setSnackbar(this.alert)
            this.planC.wod = false;  
            this.planC.wod_online = false;

        if(this.$refs.form.validate()){

            for (let i = 0; i < this.checkClassesC.length; i++) {
                if (this.checkClassesC[i]=='wod') {                                
                    this.planC.wod = true;                    
                }else if (this.checkClassesC[i]=='wod online') {
                    this.planC.wod_online = true;
                }                                                                                  
            }   
                this.createPlan(this.planC)
        }else{
            this.alert.visible = false
            this.setSnackbar(this.alert)
            this.alert.visible = true
            this.alert.message = 'Error en la creación'
            this.alert.color = 'error'
            this.alert.icon = 'mdi-close-octagon'
        }
      },
      reset () {
        this.checkClassesC.splice(0,this.checkClassesC.length);
        this.$refs.form.reset()
      },
      resetValidation () {
        this.$refs.form.resetValidation()
      },
       
       
    },
    computed: {
        ...mapGetters('plans',['getSnackbar']),
        snackbarC(){
            this.alert.visible = this.getSnackbar.visible
            this.alert.message = this.getSnackbar.message
            this.alert.color = this.getSnackbar.color
            this.alert.icon = this.getSnackbar.icon
            return this.alert
        }
    },
}