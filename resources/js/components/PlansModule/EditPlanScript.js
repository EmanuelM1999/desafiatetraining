import { mapState, mapActions, mapGetters, mapMutations } from 'vuex'

export default {
    
    props:{
        planEdited: Object,
        checkClasses: Array
    },
    data() {
        return {
            alert:{
                visible: false,
                message:'',
                color:'',
                icon:''
            },
            valid: false,
            nameRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[ñA-Za-z _]*[ñA-Za-z][ñA-Za-z _]*$/ .test(v) || 'Solo se permiten letras',
            ],
            priceRules: [
                v => !!v || 'Este es un campo obligatorio',
                v => /^[0-9]+$/.test(v) || 'Ingrese un valor valido',
            ],
            classesRules: [
                v =>  v.length > 0 || 'Selecciona al menos un tipo de clase'
            ],

            //Editar planes
            plans:[],
            plan:{
                name:'', price:'',wod:'',wod_online:''
            },
            arrayClasses:[],
            classes:[
                {value: 'wod', text: 'Wod'},
                {value: 'wod online', text: 'Wod online'}
            ],
        }
    }, 
    methods:{

        ...mapActions('plans',['dialogAction','updatePlan']),
        ...mapMutations('plans', ['setSnackbarEd']),            
        
        validate(){
       
            this.alert.visible = false
            this.setSnackbarEd(this.alert)
            this.$refs.form.validate()
            this.plan.wod = false;  
            this.plan.wod_online = false;

            if(this.$refs.form.validate()){
                for (let i = 0; i < this.arrayClasses.length; i++) {
                        if (this.arrayClasses[i]=='wod') {                                
                            this.plan.wod = true;                    
                        }else if (this.arrayClasses[i]=='wod online') {
                            this.plan.wod_online = true;
                        }                                                                                  
                }
                this.updatePlan(this.plan)   
              
            }else{
                this.alert.visible = false
                this.setSnackbarEd(this.alert)
                this.alert.visible = true
                this.alert.message = 'Error en la actualización'
                this.alert.color = 'error'
                this.alert.icon = 'mdi-close-octagon'
            }
               
        },
        reset(){
            this.arrayClasses.splice(0,this.arrayClasses.length);
        }

    },
    computed: {
         ...mapGetters('plans',
            ['getEditedPlan','getEditedClasses','getDialog','getSnackbarEd']),
        ...mapState('plans',['editedPlan']),

        snackbarEd(){
            this.alert.visible = this.getSnackbarEd.visible
            this.alert.message = this.getSnackbarEd.message
            this.alert.color = this.getSnackbarEd.color
            this.alert.icon = this.getSnackbarEd.icon
            return this.alert
        },
    },
    watch:{
        getEditedPlan:{
            handler: function(newValue) {
                this.plan.id = newValue.id
                this.plan.name = newValue.name
                this.plan.price = newValue.price
                console.log(`Cambió a ${this.plan.name} y ${this.plan.price}`);
            },
            deep: true
        },
        getEditedClasses:{
            handler: function(newValue) {
                this.arrayClasses = newValue
            },
            deep: true
        }
    },
}