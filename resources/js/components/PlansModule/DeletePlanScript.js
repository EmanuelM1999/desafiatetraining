import { mapState, mapActions, mapGetters, mapMutations } from 'vuex'
 
export default {
    data() {
        return {
            plan:{id: null, name: '',price: '',wod:'',wod_online:'' },
            alert:{
                visible: false,
                message:'',
                color:'',
                icon:''
            },
        }
    },
    methods:{
        ...mapActions('plans',['deletePlan']),
    },
    computed:{
        ...mapGetters('plans',['getSnackbarD', 'getPlanD']),

        snackbarD(){
            this.alert.visible = this.getSnackbarD.visible
            this.alert.message = this.getSnackbarD.message
            this.alert.color = this.getSnackbarD.color
            this.alert.icon = this.getSnackbarD.icon
            return this.alert
        },
        getPlan(){ 
            this.plan.id = this.getPlanD.id
            this.plan.name = this.getPlanD.name
            return this.plan
        }
    }
}
