<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least eight characters and match the confirmation.',
    'reset' => '¡Se ha reestablecido su contraseña!',
    'sent' => 'Hemos enviado un enlace de restablecimiento de contraseña por correo electrónico',
    'token' => 'This password reset token is invalid.',
    'user' => "No podemos encontrar un usuario con el correo electrónico ingresado. Verifique e intente nuevamente.",
    
];
