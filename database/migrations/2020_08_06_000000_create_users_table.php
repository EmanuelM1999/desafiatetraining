<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->bigIncrements('id')->nullable();            
            $table->string('name')->nullable();
            $table->string('surnames')->nullable();
            $table->string('photo')->nullable();
            $table->string('id_type')->nullable();
            $table->string('id_document')->nullable();
            $table->unique(['id_document','id_type']);
            $table->string('telephone')->nullable();
            $table->string('sex')->nullable();
            $table->string('age')->nullable();
            $table->timestamp('date_of_birth')->nullable();
            $table->boolean('visible_date_of_birth')->nullable();
            $table->string('home_address')->nullable();
            $table->string('city')->nullable();
            $table->string('departament')->nullable();
            $table->string('country')->nullable();
            $table->float('weight')->nullable();
            $table->float('height')->nullable();
            $table->boolean('visible_weight')->nullable();
            $table->boolean('visible_height')->nullable();
            $table->boolean('visible_benchmark_body')->nullable();
            $table->string('name_contact_emergency')->nullable();
            $table->string('telephone_contact_emergency')->nullable();
            $table->string('email')->unique()->nullable();
            $table->boolean('administrador')->nullable();
            $table->boolean('coach')->nullable();
            $table->boolean('atleta')->nullable();
            $table->BigInteger('plan_id')->unsigned()->nullable();
            $table->foreign('plan_id')->references('id')->on('plans');
            $table->date('payment_due_date')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->boolean('first_login')->nullable();
            $table->rememberToken();
            $table->timestamps();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
