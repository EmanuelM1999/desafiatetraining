<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeasurementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measurements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->float('weight');
            $table->float('height');
            $table->float('imc');
            $table->float('fat');
            $table->float('muscle');
            $table->float('caloricExpenditure');
            $table->float('metabolicAge');
            $table->float('visceralFat');
            $table->float('chest');
            $table->float('highAbdomen');
            $table->float('lowerAbdomen');
            $table->float('waist');
            $table->float('buttocks');
            $table->float('rightLeg');
            $table->float('leftLeg');
            $table->float('rightCalf');
            $table->float('leftCalf');
            $table->float('relaxedRightArm');
            $table->float('rightArmStressed');
            $table->float('relaxedLeftArm');
            $table->float('leftArmStressed');
            $table->BigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measurements');
    }
}
