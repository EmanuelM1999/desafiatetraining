<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('wod');
            $table->boolean('wod_online');
            $table->time('hour');
            $table->string('day');
            $table->BigInteger('id_template')->unsigned();
            $table->foreign('id_template')->references('id')->on('class_templates');
            $table->BigInteger('coach_id')->unsigned();                        
            $table->foreign('coach_id')->references('id')->on('users');
            $table->integer('places');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_schedules');
    }
}
