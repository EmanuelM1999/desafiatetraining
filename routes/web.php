<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/free-class-request', 'Freeclasscontroller@sendfreeclassmail');

Auth::routes();

Route::group(['middleware' => 'auth', ], function() {

    Route::resource('/login-first-time', 'FirstLoginController');        

    Route::get('/home-plans', function () {
        return view('homePlans');
    });
    
    Route::get('/home-coach', function () {
        return view('homeCoach');
    });
    

    Route::group(['middleware' => 'first_login',],function(){
        
    Route::resource('/plans', 'PlanController');
    
    Route::get('/home', 'HomeController@index')->name('home');    
    

    Route::resource('/class-template', 'ClassTemplateController');


    Route::resource('/class-schedule', 'ClassScheduleController');


    Route::resource('/user', 'UsersController');


    Route::resource('/observation', 'ObservationController')->middleware('auth');


    Route::resource('/measurements', 'MeasurementController')->middleware('auth');


    Route::resource('/payments', 'PaymentController')->middleware('auth');

    Route::get('/factura', function () {
        return view('Factura');
    });
    
    Route::get('/dashboard', function () {
        return view('dashboard');
    });
    
    Route::post('/update-rol', 'UsersController@updateRol');
    Route::post('/list-observations', 'ObservationController@listObservations');
    Route::post('/list-measurements', 'MeasurementController@listMeasurements');
    Route::post('/list-payments', 'PaymentController@listPayments');
    Route::post('/show-info-by-id', 'UsersController@showInfoById');
    Route::post('/query-classes', 'ClassScheduleController@queryClasses');    
    Route::get('/generate-PDF/{id}', 'PDFController@index');
    Route::get('/list-athletes', 'UsersController@searchAthletes');
    Route::get('/age-users', 'UsersController@ageUsers');
    Route::get('/query-coaches', 'UsersController@queryCoaches');
    Route::get('/get-user-auth', 'UsersController@getUserAuth');
    Route::get('/query-payment', 'ReserveClassController@queryPayment');
    Route::post('/update-photo', 'UsersController@updatePhoto');

    });

    
});